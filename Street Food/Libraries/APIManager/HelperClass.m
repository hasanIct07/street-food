//
//  HelperClass.m
//  TrackMyTradie
//
//  Created by bs23 on 8/29/16.
//  Copyright © 2016 bs23. All rights reserved.
//

#import "HelperClass.h"
#import "UIColor+Expanded.h"
#import "NSDate+Components.h"
#import "RNActivityView.h"

static HelperClass * sharedHelper = nil;

@implementation HelperClass{
    
    RNActivityView* hud;
    
}

+ (HelperClass*) sharedInstance {
    if (!sharedHelper) {
        sharedHelper = [[HelperClass alloc] init];
    }
    return sharedHelper;
}

- (void)errorHandler:(NSError *)error{
    NSString *msg = @"Sorry, We're having trouble to connect with our servers. Please try again.";
    if ([error localizedDescription].length > 0) {
        msg = [error localizedDescription];
    }
    
    [self showErrorMesssege:msg];
}

- (void)showHud {
    [self showHudWithText:nil];
}
- (void)hideHUd {
    if (hud) {
        [hud hide:YES];
        hud = nil;
    }
}
- (void)showHudWithText:(NSString*)string {
    [self hideHUd];
    
    hud = [RNActivityView showWithText:string ActivityViewMode:RNActivityViewModeIndeterminate];
    hud.color = [[UIColor colorWithHexString:@"#373D4A"] colorWithAlphaComponent:0.8];
    hud.indicatorColor = [[UIColor colorWithHexString:@"#E9F880"] blendWithColor:[self getUIColorObjectFromHexString:@"4A90E2" alpha:1.0f] alpha:1.0];
    hud.animationType = RNActivityViewAnimationZoomIn;
    
    
}
- (void)showErrorMesssege:(NSString*)msg {
    [self hideHUd];
    
    hud = [RNActivityView showWithText:msg ActivityViewMode:RNActivityViewModeCustomView];
    hud.labelText = msg;
    hud.mode = RNActivityViewModeCustomView;
    hud.customView = [hud getErrorView];
    [hud hide:YES afterDelay:3.0];
    hud.animationType = RNActivityViewAnimationZoomIn;
    
}

- (void)showSuccessMesssege:(NSString*)msg {
    [self hideHUd];
    
    hud = [RNActivityView showWithText:msg ActivityViewMode:RNActivityViewModeCustomView];
    hud.labelText = msg;
    hud.mode = RNActivityViewModeCustomView;
    hud.customView = [hud getSuccessView];
    [hud hide:YES afterDelay:2.0];
    hud.animationType = RNActivityViewAnimationZoomIn;
    
}



- (UIStoryboard *)getAppropriateStoryboard{
    return [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

- (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
    // Convert hex string to an integer
    unsigned int hexint = [self intFromHexString:hexStr];
    
    // Create color object, specifying alpha as well
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:alpha];
    
    return color;
}


- (unsigned int)intFromHexString:(NSString *)hexStr
{
    unsigned int hexInt = 0;
    
    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    
    // Scan hex value
    [scanner scanHexInt:&hexInt];
    
    return hexInt;
}

- (CGSize) sizeForString:(NSString*) _text withFont:(UIFont *)boundFont and:(CGSize)size {
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    NSDictionary* attributes = @{NSFontAttributeName:boundFont, NSParagraphStyleAttributeName:paragraphStyle};
    
    //    CGSize scSize =
    
    CGRect textRect = [_text boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    return textRect.size;
    
    
}

#pragma mark - EmailValidity

- (BOOL) checkingEmailValidation:(NSString*) _email
{
    
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if( ![emailTest evaluateWithObject:_email]){
        return NO;
    }
    
    return YES;
    
}






@end

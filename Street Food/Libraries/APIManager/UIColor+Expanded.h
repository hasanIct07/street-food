#import <UIKit/UIKit.h>

@interface UIColor (HexColors)

- (UIColor*)blendWithColor:(UIColor*)color2 alpha:(CGFloat)alpha2;
+ (UIColor*)colorWithHexString:(NSString*)hexString;
+ (NSString*)hexValuesFromUIColor:(UIColor*)color;

@end
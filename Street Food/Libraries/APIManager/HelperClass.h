//
//  HelperClass.h
//  TrackMyTradie
//
//  Created by bs23 on 8/29/16.
//  Copyright © 2016 bs23. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface HelperClass : NSObject
+ (HelperClass *)sharedInstance;
- (UIStoryboard *)getAppropriateStoryboard;
- (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha;
- (void)showErrorMesssege:(NSString*)msg;
- (void)showSuccessMesssege:(NSString*)msg;
- (void)showHud;
- (void)hideHUd;
- (void)showHudWithText:(NSString*)string;
- (void)errorHandler:(NSError *)error;
- (CGSize) sizeForString:(NSString*) _text withFont:(UIFont *)boundFont and:(CGSize)size;
- (BOOL) checkingEmailValidation:(NSString*) _email;
@end

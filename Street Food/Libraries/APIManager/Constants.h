//
//  Constants.h
//  TrackMyTradie
//
//  Created by bs23 on 8/29/16.
//  Copyright © 2016 bs23. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#import "AppDelegate.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define IS_IPAD ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad))
#define IS_IOS7 ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0f)
#define IS_IPHONE5 ( (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568) ? YES : NO )
#define IS_IPHONE4 ( (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height <= 480) ? YES : NO )

#define IS_IPHONE6 ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && [[UIScreen mainScreen] bounds].size.height == 667.0f) ? YES : NO )
#define IS_IPHONE6Plus ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && [[UIScreen mainScreen] bounds].size.height == 736.0f) ? YES : NO )

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define USERDEFAULTS ([NSUserDefaults standardUserDefaults])
#define DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#define DeviceFactor (IS_IPAD?2.0:1.0)

#define APPSUPPORTDIRECTORY ([NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) lastObject])
#define DocumentDirectory ([NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0])


#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
//#define kGOOGLE_API_KEY @"AIzaSyBuonDUFX8m-UKyJFMzjkSMMP4U2AvnIIE"
#define kGOOGLE_API_KEY @"AIzaSyACGQLWNQ0h-BILEepcpL8Mq1ovVkvMKaM"

#define SearchURLString @"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=geocode&language=en&key=%@"

#define AuthorizationKey @"tmt-9NRd7GR11I41Y20P0jKN146SYnzX5uMH"

#define ISLOGIN ([USERDEFAULTS boolForKey:@"LOGIN"])
#define FILTERBY ([USERDEFAULTS valueForKey:@"FILTERBY"])
#define FILTERTYPE ([USERDEFAULTS valueForKey:@"FILTERTYPE"])

#define LATITUDE ([[NSUserDefaults standardUserDefaults] valueForKey:@"LATITUDE"])
#define LONGITUDE ([[NSUserDefaults standardUserDefaults] valueForKey:@"LONGITUDE"])
#define CURRENTLOCATION ([[NSUserDefaults standardUserDefaults] valueForKey:@"CURRENTLOCATION"])


//#define BaseURLString @"http://172.16.228.158:9090/track_my_tradie_api/"
#define BaseURLString @"http://timesheet.ms-29.com/"


#define MENU_SLIDE_ANIMATION_DURATION .2
#define MENU_SLIDE_ANIMATION_OPTION UIViewAnimationOptionCurveEaseOut
#define MENU_QUICK_SLIDE_ANIMATION_DURATION .5
#define MENU_IMAGE @"menuBtn"
#define MENU_SHADOW_RADIUS 10
#define MENU_SHADOW_OPACITY 1
#define MENU_DEFAULT_SLIDE_OFFSET 90
#define MENU_FAST_VELOCITY_FOR_SWIPE_FOLLOW_DIRECTION 1200
#define STATUS_BAR_HEIGHT 20
#define NOTIFICATION_USER_INFO_MENU_LEFT @"left"
#define NOTIFICATION_USER_INFO_MENU_RIGHT @"right"
#define NOTIFICATION_USER_INFO_MENU @"menu"
#define USER_INFORMATION @"USER_INFORMATION"

//Street - Food



#endif /* Constants_h */

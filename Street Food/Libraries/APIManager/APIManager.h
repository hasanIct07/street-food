//
//  APIManager.h
//  TrackMyTradie
//
//  Created by bs23 on 8/30/16.
//  Copyright © 2016 bs23. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@protocol APIManagerDelegate;


@interface APIManager : NSObject{
    
    AFHTTPSessionManager *manager;
    
}
@property (weak, nonatomic) id <APIManagerDelegate> delegate;

+ (APIManager*) sharedManager;
- (BOOL)checkForConnectivity;
- (void)getCurrentLocation:(void (^)(BOOL))onComplete;


#pragma mark - Street Food's Method
- (void)getLocationAddressOfLat:(double)lat Long:(double)lon Completion:(void (^)(NSString* address, BOOL success ))onComplete;

- (void)getFoodWithParam:(NSDictionary *)params Completion:(void (^)(NSArray* foods, BOOL success))onComplete;

- (void)postFood:(NSDictionary *)food withPhoto:(UIImage *)image Completion:(void (^)(NSString* message, BOOL success ))onComplete;
- (void)addFoodReviewWithParam:(NSDictionary *)params Completion:(void (^)(NSString* message, BOOL success))onComplete;

#pragma mark - Others

//- (void)signUpWithUser:(NSDictionary *)info;
//- (void)signInWithUser:(NSDictionary *)info;
//- (void)signInWithFacebook:(NSDictionary *)info;
//- (void)profileUpdate:(NSDictionary *)info and:(UIImage*)photo;
//- (void)getTradieList:(NSDictionary*)info;
//- (void)addJob:(NSDictionary *)jobDetails and:(NSArray*)photos;
//- (void)editJob:(NSDictionary *)jobDetails and:(NSMutableArray*)photos;
//- (void)getJobList:(NSDictionary*)info;
//- (void)sendJobRequest:(NSDictionary*)info;
//- (void)logout:(NSDictionary*)info;
//- (void)getFavTradieList:(NSDictionary*)info;
//- (void)approveTradie:(NSDictionary*)info;
//- (void)getPendingRatingsList:(NSDictionary*)info;
//- (void)sendPinToEmailToResetPassword:(NSDictionary*)info;
//- (void)ResetPassword:(NSDictionary*)info;
//- (void)giveRatings:(NSDictionary*)info;
//- (void)suggestTradie:(NSDictionary*)info;
//- (void)suggestedTradieList:(NSDictionary*)info;
//- (void)rejectTradie:(NSDictionary*)info;
//- (void)deleteJob:(NSDictionary*)info;
//- (void)suggestTradieDelete:(NSDictionary*)info;
//- (void)getJobDetails:(NSDictionary*)info Completion:(void (^)(NSDictionary* response, NSError* error ))onComplete;
//- (void)getAllTradeCategoryCompletion:(void (^)(NSDictionary* response, NSError* error ))onComplete;
//
//- (void)makeTradieUnfav:(NSDictionary*)info Completion:(void (^)(NSDictionary* response, NSError* error ))onComplete;
//
//- (void)makeTradieFav:(NSDictionary*)info Completion:(void (^)(NSDictionary* response, NSError* error ))onComplete;


@end

@protocol APIManagerDelegate <NSObject>
@optional

#pragma mark - Street Food's Delegate


#pragma mark - Other's Delegate

- (void)signedUpSuccessfully;
- (void)signedInSuccessfully;
- (void)sentPinSuccessfully;
- (void)resetPasswordSuccessfully;
- (void)facebookSignedInSuccessfully;
- (void)loggedOutInSuccessfully;
- (void)userProfileUpdatedSuccessfully;
- (void)gotTradieList:(NSDictionary*)response;
- (void)newJobAdded:(NSDictionary*)response;
- (void)jobedited:(NSDictionary*)response;

- (void)returnJobList:(NSDictionary*)response;
- (void)jobRequestSentSuccessfully;
- (void)approveTradieSuccessfully;
- (void)tradieFavActionSuccessfully;
- (void)gotPendingRatingsList:(NSDictionary*)response;
- (void)gaveRatingsSuccessfully;
- (void)suggestedTradieSuccessfully;
- (void)rejectTradieSuccessfully;
- (void)deleteJobSuccessfully;
- (void)gotSuggestedTradieList:(NSDictionary*)response;
- (void)suggestedTradieDeleteSuccessfully;


@end

//
//  APIManager.m
//  TrackMyTradie
//
//  Created by bs23 on 8/30/16.
//  Copyright © 2016 bs23. All rights reserved.
//

#import "APIManager.h"
#import "Constants.h"
#import "Reachability.h"
#import "HelperClass.h"
//#import "UserInfo.h"
#import "Food.h"

static APIManager* sharedManger = nil;


@implementation APIManager{
    
    Reachability *internetReachability;
}

#pragma mark - sharedManager

+ (APIManager*)sharedManager {
  
    if (sharedManger == nil) {
        sharedManger = [[APIManager alloc] initWithBaseURL:[NSURL URLWithString:BaseURLString]];
        
    }
    
    return sharedManger;
}
- (instancetype)initWithBaseURL:(NSURL *)url{
    if (self = [super init]) {
        manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"text/html",@"application/json",@"application/x-www-form-urlencoded", nil]];
        
        internetReachability = [Reachability reachabilityForInternetConnection];
        [internetReachability startNotifier];
    }
    
    return self;
}

- (BOOL)checkForConnectivity {
    //    if ([Reachability reachabilityForInternetConnection]) {
    //        return YES;
    //    }
    if ([internetReachability currentReachabilityStatus]) {
        return YES;
    }
    [[HelperClass sharedInstance] showErrorMesssege:@"The Internet connection appears to be offline."];
    
    return NO;
}

#pragma mark - Street Food's Method

- (void)getLocationAddressOfLat:(double)lat Long:(double)lon Completion:(void (^)(NSString* address, BOOL success ))onComplete {
    
    NSString *urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%.7f,%.7f&sensor=false",lat,lon];
    dispatch_async(kBgQueue, ^{
        //                NSLog(@" url:%@",urlString);
        NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        NSError* error;
        if (data) {
            NSDictionary* json = [NSJSONSerialization
                                  JSONObjectWithData:data
                                  options:kNilOptions
                                  error:&error];
            NSArray* results = (NSArray*)[json objectForKey:@"results"];
            NSLog(@" results :%@",results);
            
            if (results.count > 0)
            {
                NSDictionary* addressDic = [results firstObject];
                NSString *addressStr = [addressDic valueForKey:@"formatted_address"];
                
                onComplete(addressStr, YES);
                NSLog(@"address :%@",addressDic);
            }
            else{
                onComplete(@"", NO);
            }
        }
    });
}

- (void)getFoodWithParam:(NSDictionary *)params Completion:(void (^)(NSArray* foods, BOOL success))onComplete{
    
    [[HelperClass sharedInstance] showHud];
    
    NSLog(@"info %@",params);
    
    [manager GET:@"api/food" parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject %@",responseObject);
        [[HelperClass sharedInstance] hideHUd];
        
        NSArray *foodsAry = (NSArray *)[responseObject objectForKey:@"Foods"];
        
        NSMutableArray *foodObjArray = [NSMutableArray new];
        if (foodsAry.count > 0) {
            
            for (NSDictionary * foodDic in foodsAry) {
                
                Food * foodObj = [[Food alloc] initwithServerData:foodDic];
                [foodObjArray addObject:foodObj];
            }
            
            onComplete(foodObjArray, YES);
            
        }else{
            
            [[HelperClass sharedInstance] showErrorMesssege:@"No more available"];
            onComplete(foodObjArray, NO);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[HelperClass sharedInstance] errorHandler:error];
        NSLog(@"responseObject %@",error);
        
        [[HelperClass sharedInstance] showErrorMesssege:@"Some Error!"];
        onComplete(nil, NO);
    }];
}


- (void)postFood:(NSDictionary *)food withPhoto:(UIImage *)image Completion:(void (^)(NSString* message, BOOL success ))onComplete {
    
    NSLog(@"info %@",food);
    
    [[HelperClass sharedInstance] showHudWithText:@"Uploading..."];
    
    if (image != nil) {
        
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@api/food",BaseURLString] parameters:food constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {

                CGFloat compression = 0.5f;
                CGFloat maxCompression = 0.2f;
                int maxFileSize = 640*480;
                NSData *imageData = UIImageJPEGRepresentation(image, compression);
            
                while ([imageData length] > maxFileSize && compression > maxCompression)
                {
                    compression -= 0.1;
                    imageData = UIImageJPEGRepresentation(image, compression);
                }
                
                [formData appendPartWithFileData:imageData name:@"FoodImageUrl" fileName:[NSString stringWithFormat:@"photo.jpg"] mimeType:@"image/jpg"];
        } error:nil];
        
        
        AFURLSessionManager *urlManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [urlManager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          // This is not called back on the main queue.
                          // You are responsible for dispatching to the main queue for UI updates
                          dispatch_async(dispatch_get_main_queue(), ^{
                              //Update the progress view
                              //                              [progressView setProgress:uploadProgress.fractionCompleted];
                          });
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                          if (error) {
                              NSLog(@"Error: %@", error);
                              [[HelperClass sharedInstance] errorHandler:error];
                              
                          } else {
                              NSLog(@"responseObject %@", responseObject);
                              
                              
                              if ([[responseObject objectForKey:@"Success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                                  [[HelperClass sharedInstance] showSuccessMesssege:@"Added successfully"];
                                  
                                  onComplete(@"Uploaded successfully", YES);
                              } else {
                                  
                                  [[HelperClass sharedInstance] showErrorMesssege:[responseObject objectForKey:@"Message"]];
                                  onComplete(@"Uploaded Failed", NO);
                              }
                          }
                      }];
        
        [uploadTask resume];
        
    }
    else{
        
        [manager POST:@"api/food" parameters:food progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"responseObject %@",responseObject);
            
            if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                [[HelperClass sharedInstance] showSuccessMesssege:@"Uploaded successfully"];
                
                onComplete(@"Uploaded successfully", YES);
            }
            else{
                [[HelperClass sharedInstance] showErrorMesssege:[responseObject objectForKey:@"msg"]];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            NSLog(@"responseObject %@",error);
            [[HelperClass sharedInstance] errorHandler:error];
            onComplete(@"Uploaded Failed", NO);
        }];
    }
}

- (void)addFoodReviewWithParam:(NSDictionary *)params Completion:(void (^)(NSString* message, BOOL success))onComplete{
    
    [[HelperClass sharedInstance] showHud];
    
    NSLog(@"info %@",params);
    
    [manager POST:@"api/food-review" parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject %@",responseObject);
        
        if ([[responseObject objectForKey:@"Success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            [[HelperClass sharedInstance] showSuccessMesssege:@"Review Added Successfully"];
            
            onComplete(@"Review Added Successfully Successfully", YES);
        }
        else{
            
            [[HelperClass sharedInstance] showErrorMesssege:@"Review Added Successfully Failed"];
            onComplete(@"Review Added Successfully Successfully", YES);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"responseObject %@",error);
        [[HelperClass sharedInstance] errorHandler:error];
        onComplete(@"Review Added Successfully Successfully", YES);
    }];
}


#pragma mark - UserDataMethods

/*
- (void)signUpWithUser:(NSDictionary *)info{
    
    [[HelperClass sharedInstance] showHudWithText:@"Joining..."];
    
    NSLog(@"info: %@",info);
    
    
    [manager POST:@"client_registration" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"response: %@",responseObject);

        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            
            [[HelperClass sharedInstance] showSuccessMesssege:[responseObject objectForKey:@"msg"]];
            
            UserInfo *currentUser = [UserInfo sharedUser];
            NSDictionary *tempDict = [responseObject objectForKey:@"results"];
            currentUser.userId = [tempDict objectForKey:@"userId"];
            currentUser.loginKey = [tempDict objectForKey:@"loginKey"];
            currentUser.firstName = [tempDict objectForKey:@"firstName"]?[tempDict objectForKey:@"firstName"]:@"";
            currentUser.lastName = [tempDict objectForKey:@"lastName"]?[tempDict objectForKey:@"lastName"]:@"";
            currentUser.email = [tempDict objectForKey:@"email"];
            if (![[tempDict objectForKey:@"longitude"] isEqual:[NSNull null]]) {
                
                currentUser.latitude =[tempDict objectForKey:@"latitude"];
                
            }
            else{
                currentUser.latitude =@"";
            }
            if (![[tempDict objectForKey:@"longitude"] isEqual:[NSNull null]]) {
                
                currentUser.longitude =[tempDict objectForKey:@"longitude"];
                
            }
            if (![[tempDict objectForKey:@"address"] isEqual:[NSNull null]]) {
                
                currentUser.address =[tempDict objectForKey:@"address"];
                
            }
            else{
                currentUser.address =@"";
            }
            currentUser.phoneNumber = [tempDict objectForKey:@"phone"]?[tempDict objectForKey:@"phone"]:@"";
            if (![[[tempDict objectForKey:@"photos"] valueForKey:@"width100"] isEqual:[NSNull null]]) {
                currentUser.photoUrl100 = [[tempDict objectForKey:@"photos"] valueForKey:@"width100"];
            }
            else{
                currentUser.photoUrl100 = @"";
            }
            
        
            if (![[[tempDict objectForKey:@"photos"] valueForKey:@"width250"] isEqual:[NSNull null]]) {
                currentUser.photoUrl250 = [[tempDict objectForKey:@"photos"] valueForKey:@"width250"];
            }
            else{
                currentUser.photoUrl250 = @"";
            }
            

            currentUser.status = [tempDict objectForKey:@"status"];
            [currentUser saveToUserDefaults];
            
            if ([self.delegate respondsToSelector:@selector(signedUpSuccessfully)]) {
                [self.delegate signedUpSuccessfully];
            }
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject objectForKey:@"msg"]];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"response: %@",error);
        [[HelperClass sharedInstance] errorHandler:error];

    }];
    
   
}

- (void)signInWithUser:(NSDictionary *)info{
    
    [[HelperClass sharedInstance] showHudWithText:@"Login..."];
    
    NSLog(@"info: %@",info);
    
    
    [manager POST:@"client_login" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"response: %@",responseObject);
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            
            [[HelperClass sharedInstance] showSuccessMesssege:[responseObject objectForKey:@"msg"]];
            
            UserInfo *currentUser = [UserInfo sharedUser];
            NSDictionary *tempDict = [responseObject objectForKey:@"results"];
            
            currentUser.loginKey = [tempDict objectForKey:@"loginKey"];
            currentUser.userId = [tempDict objectForKey:@"userId"];
            currentUser.firstName = [tempDict objectForKey:@"firstName"]?[tempDict objectForKey:@"firstName"]:@"";
            currentUser.lastName = [tempDict objectForKey:@"lastName"]?[tempDict objectForKey:@"lastName"]:@"";
            currentUser.email = [tempDict objectForKey:@"email"];
            if (![[tempDict objectForKey:@"address"] isEqual:[NSNull null]]) {
                
                currentUser.address =[tempDict objectForKey:@"address"];
                
            }
            else{
                currentUser.address =@"";
            }
            if (![[tempDict objectForKey:@"latitude"] isEqual:[NSNull null]]) {
                
                currentUser.latitude =[tempDict objectForKey:@"latitude"];
                
            }
            else{
                currentUser.latitude =@"";
            }
            if (![[tempDict objectForKey:@"longitude"] isEqual:[NSNull null]]) {
                
                currentUser.longitude =[tempDict objectForKey:@"longitude"];
                
            }
            else{
                currentUser.longitude =@"";
            }
            
            currentUser.phoneNumber = [tempDict objectForKey:@"phone"]?[tempDict objectForKey:@"phone"]:@"";
            if (![[[tempDict objectForKey:@"photos"] valueForKey:@"width100"] isEqual:[NSNull null]]) {
                currentUser.photoUrl100 = [[tempDict objectForKey:@"photos"] valueForKey:@"width100"];
            }
            else{
                currentUser.photoUrl100 = @"";
            }
   
            
            
            if (![[[tempDict objectForKey:@"photos"] valueForKey:@"width250"] isEqual:[NSNull null]]) {
                currentUser.photoUrl250 = [[tempDict objectForKey:@"photos"] valueForKey:@"width250"];
            }
            else{
                currentUser.photoUrl250 = @"";
            }
            currentUser.status = [tempDict objectForKey:@"status"];
            [currentUser saveToUserDefaults];
            
            if ([self.delegate respondsToSelector:@selector(signedInSuccessfully)]) {
                [self.delegate signedInSuccessfully];
            }
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject objectForKey:@"msg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"response: %@",error);
        [[HelperClass sharedInstance] errorHandler:error];
        
    }];
    
    
}

- (void)signInWithFacebook:(NSDictionary *)info{
    
    [[HelperClass sharedInstance] showHudWithText:@"Login..."];
    
    NSLog(@"info: %@",info);
    
    
    [manager POST:@"client_facebook_login" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"response: %@",responseObject);
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            
            [[HelperClass sharedInstance] showSuccessMesssege:[responseObject objectForKey:@"msg"]];
            
            UserInfo *currentUser = [UserInfo sharedUser];
            NSDictionary *tempDict = [responseObject objectForKey:@"results"];
            
            currentUser.loginKey = [tempDict objectForKey:@"loginKey"];
            currentUser.userId = [tempDict objectForKey:@"userId"];
            currentUser.firstName = [tempDict objectForKey:@"firstName"]?[tempDict objectForKey:@"firstName"]:@"";
            currentUser.lastName = [tempDict objectForKey:@"lastName"]?[tempDict objectForKey:@"lastName"]:@"";
            currentUser.email = [tempDict objectForKey:@"email"];
            if (![[tempDict objectForKey:@"address"] isEqual:[NSNull null]]) {
                
                currentUser.address =[tempDict objectForKey:@"address"];
                
            }
            else{
                currentUser.address =@"";
            }
            if (![[tempDict objectForKey:@"latitude"] isEqual:[NSNull null]]) {
                
                currentUser.latitude =[tempDict objectForKey:@"latitude"];
                
            }
            else{
                currentUser.latitude =@"";
            }
            if (![[tempDict objectForKey:@"longitude"] isEqual:[NSNull null]]) {
                
                currentUser.longitude =[tempDict objectForKey:@"longitude"];
                
            }
            else{
                currentUser.longitude =@"";
            }
            
            currentUser.phoneNumber = [tempDict objectForKey:@"phone"]?[tempDict objectForKey:@"phone"]:@"";
            if (![[[tempDict objectForKey:@"photos"] valueForKey:@"width100"] isEqual:[NSNull null]]) {
                currentUser.photoUrl100 = [[tempDict objectForKey:@"photos"] valueForKey:@"width100"];
            }
            else{
                currentUser.photoUrl100 = @"";
            }
            
            
            
            if (![[[tempDict objectForKey:@"photos"] valueForKey:@"width250"] isEqual:[NSNull null]]) {
                currentUser.photoUrl250 = [[tempDict objectForKey:@"photos"] valueForKey:@"width250"];
            }
            else{
                currentUser.photoUrl250 = @"";
            }
            currentUser.status = [tempDict objectForKey:@"status"];
            [currentUser saveToUserDefaults];
            
            if ([self.delegate respondsToSelector:@selector(facebookSignedInSuccessfully)]) {
                [self.delegate facebookSignedInSuccessfully];
            }
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject objectForKey:@"msg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"response: %@",error);
        [[HelperClass sharedInstance] errorHandler:error];
        
    }];
}

- (void)sendPinToEmailToResetPassword:(NSDictionary*)info{
    
    [[HelperClass sharedInstance] showHud];
    
    NSLog(@"info: %@",info);
    
    [manager POST:@"forget_password" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"response: %@",responseObject);
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
        
            [[HelperClass sharedInstance] hideHUd];
            
            if ([self.delegate respondsToSelector:@selector(sentPinSuccessfully)]) {
                [self.delegate sentPinSuccessfully];
            }
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject objectForKey:@"msg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"response: %@",error);
        [[HelperClass sharedInstance] errorHandler:error];
        
    }];
    
}

- (void)ResetPassword:(NSDictionary*)info{
    
    [[HelperClass sharedInstance] showHud];
    
    NSLog(@"info: %@",info);
    
    [manager POST:@"reset_password" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"response: %@",responseObject);
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            [[HelperClass sharedInstance] hideHUd];
            
            if ([self.delegate respondsToSelector:@selector(resetPasswordSuccessfully)]) {
                [self.delegate resetPasswordSuccessfully];
            }
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject objectForKey:@"msg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"response: %@",error);
        [[HelperClass sharedInstance] errorHandler:error];
        
    }];
    
}



- (void)logout:(NSDictionary*)info{
    
    [[HelperClass sharedInstance] showHudWithText:@"logout..."];
    
    NSLog(@"info: %@",info);
    
    
    [manager POST:@"client_logout" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"response: %@",responseObject);
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            
            [[HelperClass sharedInstance] showSuccessMesssege:[responseObject objectForKey:@"msg"]];
            
            UserInfo *currentUser = [UserInfo sharedUser];
            [currentUser logOutUser];
            
            if ([self.delegate respondsToSelector:@selector(loggedOutInSuccessfully)]) {
                [self.delegate loggedOutInSuccessfully];
            }
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject objectForKey:@"msg"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"response: %@",error);
        [[HelperClass sharedInstance] errorHandler:error];
        
    }];

}

- (void)profileUpdate:(NSDictionary *)info and:(UIImage*)photo{
    

    NSLog(@"info %@",info);
    
    if (photo) {
        NSData *imageData;
        CGFloat compression = 1.0f;
        CGFloat minCompression = 0.1f;
        int maxFileSize = 200*200;
        imageData = UIImageJPEGRepresentation(photo, compression);
        
        while ([imageData length] > maxFileSize && compression > minCompression)
        {
            compression -= 0.1;
            imageData = UIImageJPEGRepresentation(photo, compression);
        }
        
        [manager POST:@"client_profile_update" parameters:info constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
             [formData appendPartWithFileData:imageData name:@"photo" fileName:@"profile_photo.jpg" mimeType:@"image/jpg"];
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"update_profile  ResponseObject---------%@",responseObject);

            if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                
                
                UserInfo *currentUser = [UserInfo sharedUser];
                NSDictionary *tempDict = [responseObject objectForKey:@"results"];
                
                currentUser.firstName = [tempDict objectForKey:@"firstName"]?[tempDict objectForKey:@"firstName"]:@"";
                currentUser.lastName = [tempDict objectForKey:@"lastName"]?[tempDict objectForKey:@"lastName"]:@"";
                currentUser.email = [tempDict objectForKey:@"email"];
                if (![[tempDict objectForKey:@"address"] isEqual:[NSNull null]]) {
                    
                    currentUser.address =[tempDict objectForKey:@"address"];
                    
                }
                else{
                    currentUser.address =@"";
                }
                if (![[tempDict objectForKey:@"latitude"] isEqual:[NSNull null]]) {
                    
                    currentUser.latitude =[tempDict objectForKey:@"latitude"];
                    
                }
                else{
                    currentUser.latitude =@"";
                }
                if (![[tempDict objectForKey:@"longitude"] isEqual:[NSNull null]]) {
                    
                    currentUser.longitude =[tempDict objectForKey:@"longitude"];
                    
                }
                else{
                    currentUser.longitude =@"";
                }
                currentUser.phoneNumber = [tempDict objectForKey:@"phone"]?[tempDict objectForKey:@"phone"]:@"";
                if (![[[tempDict objectForKey:@"photos"] valueForKey:@"width100"] isEqual:[NSNull null]]) {
                    currentUser.photoUrl100 = [[tempDict objectForKey:@"photos"] valueForKey:@"width100"];
                }
                else{
                    currentUser.photoUrl100 = @"";
                }
                
                if (![[[tempDict objectForKey:@"photos"] valueForKey:@"width250"] isEqual:[NSNull null]]) {
                    currentUser.photoUrl250 = [[tempDict objectForKey:@"photos"] valueForKey:@"width250"];
                }
                else{
                    currentUser.photoUrl250 = @"";
                }
                
                currentUser.status = [tempDict objectForKey:@"status"];
                
                [currentUser saveToUserDefaults];
                
                [[HelperClass sharedInstance] showSuccessMesssege:@"Updated successfully"];
                if ([self.delegate respondsToSelector:@selector(userProfileUpdatedSuccessfully)]) {
                    [self.delegate userProfileUpdatedSuccessfully];
                }
            }
            else{
                [[HelperClass sharedInstance] showErrorMesssege:[responseObject objectForKey:@"msg"]];
            }
            

            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [[HelperClass sharedInstance] errorHandler:error];

        }];
        
    }
    else{
        
        [manager POST:@"client_profile_update" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"responseObject %@",responseObject);

            if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                
                [[HelperClass sharedInstance] showSuccessMesssege:[responseObject objectForKey:@"msg"]];
                UserInfo *currentUser = [UserInfo sharedUser];
                NSDictionary *tempDict = [responseObject objectForKey:@"results"];
                
                currentUser.firstName = [tempDict objectForKey:@"firstName"]?[tempDict objectForKey:@"firstName"]:@"";
                currentUser.lastName = [tempDict objectForKey:@"lastName"]?[tempDict objectForKey:@"lastName"]:@"";
                currentUser.email = [tempDict objectForKey:@"email"];
                if (![[tempDict objectForKey:@"address"] isEqual:[NSNull null]]) {
                    
                    currentUser.address =[tempDict objectForKey:@"address"];
                    
                }
                else{
                    currentUser.address =@"";
                }
                if (![[tempDict objectForKey:@"latitude"] isEqual:[NSNull null]]) {
                    
                    currentUser.latitude =[tempDict objectForKey:@"latitude"];
                    
                }
                else{
                    currentUser.latitude =@"";
                }
                if (![[tempDict objectForKey:@"longitude"] isEqual:[NSNull null]]) {
                    
                    currentUser.longitude =[tempDict objectForKey:@"longitude"];
                    
                }
                else{
                    currentUser.longitude =@"";
                }
                currentUser.phoneNumber = [tempDict objectForKey:@"phone"]?[tempDict objectForKey:@"phone"]:@"";
                if (![[[tempDict objectForKey:@"photos"] valueForKey:@"width100"] isEqual:[NSNull null]]) {
                    currentUser.photoUrl100 = [[tempDict objectForKey:@"photos"] valueForKey:@"width100"];
                }
                else{
                    currentUser.photoUrl100 = @"";
                }
                
                if (![[[tempDict objectForKey:@"photos"] valueForKey:@"width250"] isEqual:[NSNull null]]) {
                    currentUser.photoUrl250 = [[tempDict objectForKey:@"photos"] valueForKey:@"width250"];
                }
                else{
                    currentUser.photoUrl250 = @"";
                }
                
                currentUser.status = [tempDict objectForKey:@"status"];
                
                [currentUser saveToUserDefaults];
                
                if ([self.delegate respondsToSelector:@selector(userProfileUpdatedSuccessfully)]) {
                    [self.delegate userProfileUpdatedSuccessfully];
                }
            }
            else{
                [[HelperClass sharedInstance] showErrorMesssege:[responseObject objectForKey:@"msg"]];
            }

        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [[HelperClass sharedInstance] errorHandler:error];
            NSLog(@"responseObject %@",error);

        }];
    }
    
    
    
}
*/


- (void)getCurrentLocation:(void (^)(BOOL))onComplete {
    NSString *urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%@,%@&sensor=false",LATITUDE,LONGITUDE];
    dispatch_async(kBgQueue, ^{
        //                NSLog(@" url:%@",urlString);
        NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        NSError* error;
        if (data) {
            NSDictionary* json = [NSJSONSerialization
                                  JSONObjectWithData:data
                                  options:kNilOptions
                                  error:&error];
            NSArray* results = (NSArray*)[json objectForKey:@"results"];
            NSLog(@" results :%@",results);
            
            if (results.count > 0)
            {
                NSDictionary* address = [results firstObject];
                [USERDEFAULTS setValue:[address valueForKey:@"formatted_address"] forKey:@"CURRENTLOCATION"];
                [USERDEFAULTS synchronize];
                onComplete(YES);
                NSLog(@" address :%@",address);
                
                
            }
            else{
                [USERDEFAULTS setValue:@"" forKey:@"CURRENTLOCATION"];
                [USERDEFAULTS synchronize];
                onComplete(NO);
                
            }

        }
    });
}



#pragma mark - JobMethods


- (void)addJob:(NSDictionary *)jobDetails and:(NSMutableArray*)photos{
    
    NSLog(@"info %@",jobDetails);
    
    [[HelperClass sharedInstance] showHudWithText:@"Uploading..."];
    
    if (photos.count>0) {
        
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@add_job",BaseURLString] parameters:jobDetails constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            int index = 0;
            for (UIImage* _image in photos) {
                
                NSLog(@"index %d",index);
             
                CGFloat compression = 0.5f;
                CGFloat maxCompression = 0.2f;
                int maxFileSize = 640*480;
                NSData *imageData = UIImageJPEGRepresentation(_image, compression);
                
                
                while ([imageData length] > maxFileSize && compression > maxCompression)
                {
                    compression -= 0.1;
                    imageData = UIImageJPEGRepresentation(_image, compression);
                }
                
                [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"photos[%d]",index] fileName:[NSString stringWithFormat:@"photo.jpg"] mimeType:@"image/jpg"];
                
                index++;
            }
        } error:nil];
        
        
        AFURLSessionManager *urlManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [urlManager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          // This is not called back on the main queue.
                          // You are responsible for dispatching to the main queue for UI updates
                          dispatch_async(dispatch_get_main_queue(), ^{
                              //Update the progress view
//                              [progressView setProgress:uploadProgress.fractionCompleted];
                          });
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                          if (error) {
                              NSLog(@"Error: %@", error);
                              [[HelperClass sharedInstance] errorHandler:error];

                          } else {
                              NSLog(@"responseObject %@", responseObject);
                              
                              
                if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                    [[HelperClass sharedInstance] showSuccessMesssege:@"Uploaded successfully"];

                    
                        if ([self.delegate respondsToSelector:@selector(newJobAdded:)]) {
                            [self.delegate newJobAdded:responseObject];
                            
                        }
                            }
                        else
                              {
                                  [[HelperClass sharedInstance] showErrorMesssege:[responseObject objectForKey:@"msg"]];
                              }

                          }
                      }];
        
        [uploadTask resume];
        
    }
    else{
        
        [manager POST:@"add_job" parameters:jobDetails progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"responseObject %@",responseObject);
            
            if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                [[HelperClass sharedInstance] showSuccessMesssege:@"Uploaded successfully"];

                if ([self.delegate respondsToSelector:@selector(newJobAdded:)]) {
                    [self.delegate newJobAdded:responseObject];
                }
            }
            else{
                [[HelperClass sharedInstance] showErrorMesssege:[responseObject objectForKey:@"msg"]];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [[HelperClass sharedInstance] errorHandler:error];
            NSLog(@"responseObject %@",error);
            
        }];
    }
    
    
    
}

- (void)editJob:(NSDictionary *)jobDetails and:(NSMutableArray*)photos{
    
    NSLog(@"info %@",jobDetails);
    
    [[HelperClass sharedInstance] showHudWithText:@"Uploading..."];
    
    if (photos.count>0) {
        
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@edit_job",BaseURLString] parameters:jobDetails constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            int index = 0;
            for (UIImage* _image in photos) {
                
                NSLog(@"index %d",index);
                
                CGFloat compression = 0.5f;
                CGFloat maxCompression = 0.2f;
                int maxFileSize = 640*480;
                NSData *imageData = UIImageJPEGRepresentation(_image, compression);
                
                
                while ([imageData length] > maxFileSize && compression > maxCompression)
                {
                    compression -= 0.1;
                    imageData = UIImageJPEGRepresentation(_image, compression);
                }
                
                [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"photos[%d]",index] fileName:[NSString stringWithFormat:@"photo.jpg"] mimeType:@"image/jpg"];
                
                index++;
            }
        } error:nil];
        
        
        AFURLSessionManager *urlManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [urlManager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          // This is not called back on the main queue.
                          // You are responsible for dispatching to the main queue for UI updates
                          dispatch_async(dispatch_get_main_queue(), ^{
                              //Update the progress view
                              //                              [progressView setProgress:uploadProgress.fractionCompleted];
                          });
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                          if (error) {
                              NSLog(@"Error: %@", error);
                              [[HelperClass sharedInstance] errorHandler:error];
                              
                          } else {
                              NSLog(@"responseObject %@", responseObject);
                              
                              
                              if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                                  [[HelperClass sharedInstance] showSuccessMesssege:@"Uploaded successfully"];
                                  
                                  
                                  if ([self.delegate respondsToSelector:@selector(jobedited:)]) {
                                      [self.delegate jobedited:[responseObject valueForKey:@"results"]];
                                      
                                  }
                              }
                              else
                              {
                                  [[HelperClass sharedInstance] showErrorMesssege:[responseObject objectForKey:@"msg"]];
                              }
                              
                          }
                      }];
        
        [uploadTask resume];
        
    }
    else{
        
        [manager POST:@"edit_job" parameters:jobDetails progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"responseObject %@",responseObject);
            
            if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                [[HelperClass sharedInstance] showSuccessMesssege:@"Uploaded successfully"];
                
                if ([self.delegate respondsToSelector:@selector(jobedited:)]) {
                    [self.delegate jobedited:[responseObject valueForKey:@"results"]];
                }
            }
            else{
                [[HelperClass sharedInstance] showErrorMesssege:[responseObject objectForKey:@"msg"]];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [[HelperClass sharedInstance] errorHandler:error];
            NSLog(@"responseObject %@",error);
            
        }];
    }
    
    
    
}


- (void)getJobList:(NSDictionary*)info{
    
    [[HelperClass sharedInstance] showHud];
    
    NSLog(@"info %@",info);
    
    [manager POST:@"client_job_list" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject %@",responseObject);
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            if ([self.delegate respondsToSelector:@selector(returnJobList:)]) {
                [self.delegate returnJobList:responseObject];
            }
            
            [[HelperClass sharedInstance] hideHUd];
            
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject valueForKey:@"msg"]];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[HelperClass sharedInstance] errorHandler:error];
        NSLog(@"responseObject %@",error);
        
    }];
}

- (void)sendJobRequest:(NSDictionary*)info{
    
    [[HelperClass sharedInstance] showHud];
    
    NSLog(@"info %@",info);
    
    [manager POST:@"assign_trade_person" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject %@",responseObject);
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            if ([self.delegate respondsToSelector:@selector(jobRequestSentSuccessfully)]) {
                [self.delegate jobRequestSentSuccessfully];
            }
            
            [[HelperClass sharedInstance] hideHUd];
            
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject valueForKey:@"msg"]];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[HelperClass sharedInstance] errorHandler:error];
        NSLog(@"responseObject %@",error);
        
    }];

    
}

- (void)getJobDetails:(NSDictionary*)info Completion:(void (^)(NSDictionary* response, NSError* error ))onComplete{
    
    [[HelperClass sharedInstance] showHud];

    
    [manager POST:@"job_details" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"response: %@",responseObject);
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            onComplete(responseObject, nil);
            [[HelperClass sharedInstance] hideHUd];

        }
        else{
            onComplete(nil, nil);
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject valueForKey:@"msg"]];

        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"response: %@",error);
        onComplete(nil, error);
        [[HelperClass sharedInstance] errorHandler:error];

        
        
    }];

}

- (void)approveTradie:(NSDictionary*)info{

    
    [[HelperClass sharedInstance] showHud];
    
    NSLog(@"info %@",info);
    
    [manager POST:@"hire_trade_person" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject %@",responseObject);
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            if ([self.delegate respondsToSelector:@selector(approveTradieSuccessfully)]) {
                [self.delegate approveTradieSuccessfully];
            }
            
            [[HelperClass sharedInstance] hideHUd];
            
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject valueForKey:@"msg"]];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[HelperClass sharedInstance] errorHandler:error];
        NSLog(@"responseObject %@",error);
        
    }];

}

- (void)rejectTradie:(NSDictionary*)info{
    
    
    [[HelperClass sharedInstance] showHud];
    
    NSLog(@"info %@",info);
    
    [manager POST:@"reject_trade_person" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject %@",responseObject);
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            if ([self.delegate respondsToSelector:@selector(rejectTradieSuccessfully)]) {
                [self.delegate rejectTradieSuccessfully];
            }
            
            [[HelperClass sharedInstance] hideHUd];
            
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject valueForKey:@"msg"]];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[HelperClass sharedInstance] errorHandler:error];
        NSLog(@"responseObject %@",error);
        
    }];
    
}



- (void)deleteJob:(NSDictionary*)info{
    
    
    [[HelperClass sharedInstance] showHud];
    
    NSLog(@"info %@",info);
    
    [manager POST:@"delete_job" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject %@",responseObject);
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            if ([self.delegate respondsToSelector:@selector(deleteJobSuccessfully)]) {
                [self.delegate deleteJobSuccessfully];
            }
            
            [[HelperClass sharedInstance] hideHUd];
            
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject valueForKey:@"msg"]];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[HelperClass sharedInstance] errorHandler:error];
        NSLog(@"responseObject %@",error);
        
    }];
    
}




#pragma mark - TradeCategory

- (void)getAllTradeCategoryCompletion:(void (^)(NSDictionary* response, NSError* error ))onComplete{

    
    [manager POST:@"category_list" parameters:@{@"authKey":AuthorizationKey} progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"response: %@",responseObject);

        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            onComplete(responseObject, nil);

        }
        else{
            onComplete(nil, nil);

        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"response: %@",error);
        onComplete(nil, error);

        
    }];
    
    
}

#pragma mark - TradieMethods

- (void)getTradieList:(NSDictionary*)info{
    
    [[HelperClass sharedInstance] showHud];
    
    NSLog(@"info %@",info);
    
    
    [manager POST:@"trade_person_list" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            if ([self.delegate respondsToSelector:@selector(gotTradieList:)]) {
                [self.delegate gotTradieList:responseObject];
            }
            
            [[HelperClass sharedInstance] hideHUd];

        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject valueForKey:@"msg"]];

        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[HelperClass sharedInstance] errorHandler:error];
        NSLog(@"responseObject %@",error);

    }];
}

- (void)makeTradieFav:(NSDictionary*)info Completion:(void (^)(NSDictionary* response, NSError* error ))onComplete{
    
    [manager POST:@"make_favorite_trade_person" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"response: %@",responseObject);
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            onComplete(responseObject, nil);
            
        }
        else{
            onComplete(nil, nil);
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"response: %@",error);
        onComplete(nil, error);
        
        
    }];

}

- (void)makeTradieUnfav:(NSDictionary*)info Completion:(void (^)(NSDictionary* response, NSError* error ))onComplete{
    
    [manager POST:@"remove_favorite_trade_person" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"response: %@",responseObject);
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            onComplete(responseObject, nil);
            
        }
        else{
            onComplete(nil, nil);
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"response: %@",error);
        onComplete(nil, error);
        
        
    }];
    
}


- (void)getFavTradieList:(NSDictionary*)info{
    
    [[HelperClass sharedInstance] showHud];
    
    
    [manager POST:@"favorite_trade_persons" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            if ([self.delegate respondsToSelector:@selector(gotTradieList:)]) {
                [self.delegate gotTradieList:responseObject];
            }
            
            [[HelperClass sharedInstance] hideHUd];
            
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject valueForKey:@"msg"]];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[HelperClass sharedInstance] errorHandler:error];
        NSLog(@"responseObject %@",error);
        
    }];
}

#pragma mark - SuggestTradieMethod

- (void)suggestedTradieList:(NSDictionary*)info{
    
    [[HelperClass sharedInstance] showHud];
    
    
    [manager POST:@"suggested_trade_person_list" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject %@", responseObject);
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            if ([self.delegate respondsToSelector:@selector(gotSuggestedTradieList:)]) {
                [self.delegate gotSuggestedTradieList:responseObject];
            }
            
            [[HelperClass sharedInstance] hideHUd];
            
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject valueForKey:@"msg"]];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[HelperClass sharedInstance] errorHandler:error];
        NSLog(@"responseObject %@",error);
        
    }];
}

- (void)suggestTradie:(NSDictionary*)info{
    
    [[HelperClass sharedInstance] showHud];
    
    
    [manager POST:@"suggest_trade_person" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject %@", responseObject);
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            if ([self.delegate respondsToSelector:@selector(suggestedTradieSuccessfully)]) {
                [self.delegate suggestedTradieSuccessfully];
            }
            
            [[HelperClass sharedInstance] hideHUd];
            
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject valueForKey:@"msg"]];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[HelperClass sharedInstance] errorHandler:error];
        NSLog(@"responseObject %@",error);
        
    }];
}

- (void)suggestTradieDelete:(NSDictionary*)info{
    
    [[HelperClass sharedInstance] showHud];
    
    
    [manager POST:@"delete_suggested_trade_person" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject %@", responseObject);
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            if ([self.delegate respondsToSelector:@selector(suggestedTradieDeleteSuccessfully)]) {
                [self.delegate suggestedTradieDeleteSuccessfully];
            }
            
            [[HelperClass sharedInstance] hideHUd];
            
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject valueForKey:@"msg"]];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[HelperClass sharedInstance] errorHandler:error];
        NSLog(@"responseObject %@",error);
        
    }];
}


#pragma mark - Ratings

- (void)getPendingRatingsList:(NSDictionary*)info{
    
    [[HelperClass sharedInstance] showHud];
    
    
    [manager POST:@"trade_persons_pending_rating" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject %@", responseObject);
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            if ([self.delegate respondsToSelector:@selector(gotPendingRatingsList:)]) {
                [self.delegate gotPendingRatingsList:responseObject];
            }
            
            [[HelperClass sharedInstance] hideHUd];
            
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject valueForKey:@"msg"]];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[HelperClass sharedInstance] errorHandler:error];
        NSLog(@"responseObject %@",error);
        
    }];
}

- (void)giveRatings:(NSDictionary*)info{
    
    [[HelperClass sharedInstance] showHud];
    
    
    [manager POST:@"set_rating" parameters:info progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject %@", responseObject);
        if ([[responseObject objectForKey:@"success"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            if ([self.delegate respondsToSelector:@selector(gaveRatingsSuccessfully)]) {
                [self.delegate gaveRatingsSuccessfully];
            }
            
            [[HelperClass sharedInstance] hideHUd];
            
        }
        else{
            [[HelperClass sharedInstance] showErrorMesssege:[responseObject valueForKey:@"msg"]];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[HelperClass sharedInstance] errorHandler:error];
        NSLog(@"responseObject %@",error);
        
    }];
}









@end

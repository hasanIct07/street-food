//
//  NSDate+Components.m
//  menu123
//
//  Created by Shah Newaz Hossain on 8/14/15.
//  Copyright (c) 2015 Shah Newaz Hossain. All rights reserved.
//


#import "NSDate+Components.h"


@implementation NSDate (Components)

#pragma mark - Convenience Intializers

+ (NSDate *)dateWithDateString:(NSString*)dString {
    NSArray* ar = [dString componentsSeparatedByString:@" "];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    if (ar.count > 0) {
        NSArray* dateAr = [ar[0] componentsSeparatedByString:@"-"];
        if (dateAr.count > 0) {
            [comps setYear:[dateAr[0] integerValue]];
        }
        if (dateAr.count > 1) {
            [comps setMonth:[dateAr[1] integerValue]];
        }
        if (dateAr.count > 2) {
            [comps setDay:[dateAr[2] integerValue]];
        }
    }
    if (ar.count > 1) {
        NSArray* dateAr = [ar[1] componentsSeparatedByString:@":"];
        if (dateAr.count > 0) {
            [comps setHour:[dateAr[0] integerValue]];
        }
        if (dateAr.count > 1) {
            [comps setMinute:[dateAr[1] integerValue]];
        }
        if (dateAr.count > 2) {
            [comps setSecond:[dateAr[2] integerValue]];
        }
    }
    NSCalendar *cal = [NSCalendar currentCalendar];//[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    [cal setTimeZone:[NSTimeZone localTimeZone]];
    return [cal dateFromComponents:comps];
    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm"];
//    NSDate *myDate = [dateFormatter dateFromString:dString];
//    NSLog(@"%@", [dateFormatter stringFromDate:myDate]);
//    return myDate;
}
+ (NSDate *)dateWithDay:(NSUInteger)day month:(NSUInteger)month year:(NSUInteger)year
{
    return [NSDate dateWithDay:day Month:month Year:year andCalendar:[NSCalendar currentCalendar]];
}

+ (NSDate *)dateWithDay:(NSUInteger)day Month:(NSUInteger)month Year:(NSUInteger)year andCalendar:(NSCalendar *)calendar
{
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:day];
    [components setMonth:month];
    [components setYear:year];
    
    return [calendar dateFromComponents:components];
}

//  The "everything" method

+ (NSDate *)dateWithEra:(NSInteger)era year:(NSInteger)year month:(NSInteger)month day:(NSInteger)day hour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second week:(NSInteger)week weekday:(NSInteger)weekday weekdayOrdinal:(NSInteger)weekdayOrdinal andCalendar:(NSCalendar *)calendar
{
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setEra:era];
    [components setYear:year];
    [components setMonth:month];
    [components setDay:day];
    [components setHour:hour];
    [components setMinute:minute];
    [components setSecond:second];
    [components setWeekOfYear:week];
    [components setWeekday:weekday];
    [components setWeekdayOrdinal:weekdayOrdinal];
    
    return [calendar dateFromComponents:components];
}

#pragma mark - Default Values

//    All default values return components of [NSDate date].
+ (NSInteger)defaultEraForCalendar:(NSCalendar *)calendar
{
    return [[NSDate defaultComponentsForCalendar:calendar] era];
}

+ (NSInteger)defaultYearForCalendar:(NSCalendar *)calendar
{
    return [[NSDate defaultComponentsForCalendar:calendar] year];
}

+ (NSInteger)defaultMonthForCalendar:(NSCalendar *)calendar
{
    return [[NSDate defaultComponentsForCalendar:calendar] month];
}

+ (NSInteger)defaultDayForCalendar:(NSCalendar *)calendar
{
    return [[NSDate defaultComponentsForCalendar:calendar] day];
}

+ (NSInteger)defaultHourForCalendar:(NSCalendar *)calendar
{
    return [[NSDate defaultComponentsForCalendar:calendar] hour];
}

+ (NSInteger)defaultMinuteForCalendar:(NSCalendar *)calendar
{
    return [[NSDate defaultComponentsForCalendar:calendar] minute];
}

+ (NSInteger)defaultSecondForCalendar:(NSCalendar *)calendar
{
    return [[NSDate defaultComponentsForCalendar:calendar] second];
}

+ (NSInteger)defaultWeekForCalendar:(NSCalendar *)calendar
{
    return [[NSDate defaultComponentsForCalendar:calendar] weekOfYear];
}

+ (NSInteger)defaultWeekdayForCalendar:(NSCalendar *)calendar
{
    return [[NSDate defaultComponentsForCalendar:calendar] weekday];
}

+ (NSInteger)defaultWeekdayOrdinalForCalendar:(NSCalendar *)calendar
{
    return [[NSDate defaultComponentsForCalendar:calendar] weekdayOrdinal];
}

//  These use [NSCalendar currentCalendar].
+ (NSInteger)defaultEra
{
    return [[NSDate defaultComponents] era];
}

+ (NSInteger)defaultYear
{
    return [[NSDate defaultComponents] year];
}

+ (NSInteger)defaultMonth
{
    return [[NSDate defaultComponents] month];
}

+ (NSInteger)defaultDay
{
    return [[NSDate defaultComponents] day];
}

+ (NSInteger)defaultHour
{
    return [[NSDate defaultComponents] hour];
}

+ (NSInteger)defaultMinute
{
    return [[NSDate defaultComponents] minute];
}

+ (NSInteger)defaultSecond
{
    return [[NSDate defaultComponents] second];
}

+ (NSInteger)defaultWeek
{
    return [[NSDate defaultComponents] weekOfYear];
}

+ (NSInteger)defaultWeekday
{
    return [[NSDate defaultComponents] weekday];
}

+ (NSInteger)defaultWeekdayOrdinal
{
    return [[NSDate defaultComponents] weekdayOrdinal];
}

#pragma mark - Default Components and Calendar

+ (NSDateComponents *)defaultComponents
{
    return [self defaultComponentsForCalendar:[NSCalendar currentCalendar]];
}

+ (NSDateComponents *)defaultComponentsForCalendar:(NSCalendar *)calendar
{    
    return [calendar components:[NSDateComponents allComponents] fromDate:[NSDate date]];
}

- (NSString *)stringWithCustomFormat:(NSString*)format {
    return [self stringWithCustomFormat:format ForCalendar:[NSCalendar currentCalendar]];
}
- (NSString *)stringWithCustomFormat:(NSString*)format ForCalendar:(NSCalendar *)calendar {
    NSDateFormatter *f = [NSDateFormatter new];
    [f setCalendar:calendar];
    [f setDateFormat:format];
    return [f stringFromDate:self];

}
@end

@implementation NSDateComponents (AllComponents)

#pragma mark - All Components

+ (NSUInteger)allComponents
{
    return (NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday);

//    return (NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekCalendarUnit);
}

@end

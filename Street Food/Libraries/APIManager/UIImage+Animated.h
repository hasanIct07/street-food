//
//  UIImage+Animated.h
//  CheckMe
//
//  Created by TM Mac 01 on 1/28/15.
//  Copyright (c) 2015 ShahNewazHossain. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIImage (Animated)
+ (UIImage *)screenshot;
+ (UIImage *)screenshotWithSize:(CGSize)imageCropSize;
+ (void) loadImageFromURL: (NSURL*) url callback:(void (^)(UIImage *image))callback;
+ (void) loadDataFromURL: (NSURL*) url callback:(void (^)(NSData *imageData))callback;

+ (UIImage *)animatedImageWithAnimatedGIFData:(NSData *)theData;

+ (UIImage *)animatedImageWithAnimatedGIFURL:(NSURL *)theURL;

+ (UIImage *)animatedImageWithAnimatedGIFName:(NSString *)name;

- (UIImage *)blurredImageWithRadius:(CGFloat)radius iterations:(NSUInteger)iterations tintColor:(UIColor *)tintColor;
- (UIImage *)convertImageToGrayScale:(UIImage *)image;

@end

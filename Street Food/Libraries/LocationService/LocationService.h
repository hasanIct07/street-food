//
//  LocationService.h
//  IOTDemo
//
//  Created by Mehedi Hasan on 3/9/16.
//  Copyright © 2016 Mehedi Hasan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol LocationServiceDelegate <NSObject>

-(void)didUpdateLocationsOfLocationService:(CLLocationCoordinate2D)coordinate;
@end

@interface LocationService : NSObject <CLLocationManagerDelegate>


//+(LocationService *) sharedInstance;

+ (instancetype) sharedInstance;

@property (strong, nonatomic) id<LocationServiceDelegate> delegate;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;

- (void)startLocationService;
- (void)stopLocationService;

-(CLLocationCoordinate2D)getMyCoordinate;
-(NSString *)getDistanceFromLat:(double)latitude AndLong:(double) longitude;


@end

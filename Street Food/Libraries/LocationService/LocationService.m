//
//  LocationService.m
//  IOTDemo
//
//  Created by Mehedi Hasan on 3/9/16.
//  Copyright © 2016 Mehedi Hasan. All rights reserved.
//

#import "LocationService.h"

@interface LocationService()

@property (nonatomic) BOOL isObserving;


@end


@implementation LocationService

+ (instancetype)sharedInstance {
    
    static LocationService *instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        instance = [[LocationService alloc] init];
    });
    
    return instance;
}


- (instancetype)init {
    
    self = [super init];
    self.isObserving = NO;
    
    if(self){
        
        // For better Battery life, tunes these value to more
        // distanceFilter = kCLDistanceFilterNone, 30, 100  Meters.
        //desiredAccuracy = kCLLocationAccuracyHundredMeters, kCLLocationAccuracyBest
        
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.distanceFilter = 100.0;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        self.locationManager.delegate = self;
        
        if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            
            //Note this one
            [self.locationManager requestWhenInUseAuthorization];
        }

        //start Updating Location
        [self startLocationService];
        //self.currentLocation = self.locationManager.location;
    }
    
    return  self;
}

- (void) startLocationService {
    
    if(self.isObserving == NO){
        self.isObserving = YES;
        
        NSLog(@"Starting location updates");
        [self.locationManager startUpdatingLocation];
    }
}

- (void)stopLocationService {
    
    if(self.isObserving){
        self.isObserving = NO;
        
        //NSLog(@"Stopping location updates");
        [self.locationManager stopUpdatingLocation];
    }
}

//


-(CLLocationCoordinate2D)getMyCoordinate{
    
    CLLocationCoordinate2D coordinate = self.currentLocation.coordinate;
    
    if (CLLocationCoordinate2DIsValid(coordinate)) {
       
        return coordinate;
    }else{

        return CLLocationCoordinate2DMake(0, 0);
    }
}

-(NSString *)getDistanceFromLat:(double)latitude AndLong:(double) longitude{

    //NSString *distanceString;
    
    if(latitude == 0 && longitude == 0){
        return @"-";
    }
    
    if(self.locationManager){
        
        CLLocation *dictance = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];

        CLLocationDistance distance = [self.currentLocation distanceFromLocation:dictance];
        
        float distMile = distance/1609.34;
        
        NSString *distanceString = [NSString stringWithFormat:@"%.2f Mile", distMile];
        
        return distanceString;
    }
    
    return @"-";
}





#pragma mark - Location Manager

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {

    //NSLog(@"called didUpdateLocations");
    
    self.currentLocation = [locations lastObject];
    
    //NSString * latitude = [NSString stringWithFormat:@"%.7f", self.currentLocation.coordinate.latitude];
    //NSString * longitude = [NSString stringWithFormat:@"%.7f", self.currentLocation.coordinate.longitude];
    
    
    NSLog(@"Lat:\t %.7f\t Long:\t %.7f", self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude);
    
    
    //NSNumber *lat = [NSNumber numberWithDouble:self.currentLocation.coordinate.latitude];
    //NSNumber *lon = [NSNumber numberWithDouble:self.currentLocation.coordinate.longitude];
    //NSDictionary *userLocationDic =@{@"lat":lat,@"long":lon};
    //NSDictionary *userInfoDic = [NSDictionary dictionaryWithObject:userLocationDic forKey:@"userLocationDic"];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_LOCATION_NOTIFICATION object:self userInfo:userInfoDic];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    NSLog(@"Location service failed with error %@", error);
}





@end

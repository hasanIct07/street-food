//
//  FoodAnnotation.h
//  Street Food
//
//  Created by Mehedi Hasan on 2/10/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Food.h"

@interface FoodAnnotation : NSObject <MKAnnotation> {

}

- (instancetype)initwithFood:(Food*)food;

@property (nonatomic, retain) Food *food;
@property (nonatomic) CLLocationCoordinate2D coordinate;

@end

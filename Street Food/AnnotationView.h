//
//  AnnotationView.h
//  Street Food
//
//  Created by Mehedi Hasan on 2/10/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface AnnotationView : MKAnnotationView

@property (weak, nonatomic) UIView *calloutView;
@property (weak, nonatomic) UIImageView *imageView;
@property (weak, nonatomic) UILabel *label;


@property (weak, nonatomic) IBOutlet UIImageView *contentImageView;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;


@end

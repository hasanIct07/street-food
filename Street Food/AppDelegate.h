//
//  AppDelegate.h
//  Street Food
//
//  Created by Mehedi Hasan on 1/14/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


//
//  DetailsViewController.h
//  Street Food
//
//  Created by Mehedi Hasan on 1/21/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KIImagePager.h"
#import "HCSStarRatingView.h"
#import "Food.h"
@import MapKit;

@interface DetailsViewController : UIViewController <MKMapViewDelegate>

@property (nonatomic, strong) Food *selectedFood;


@property NSString *foodImageName;
@property NSArray *foodImageAry;
@property (nonatomic) CLLocationCoordinate2D shopCoordinate;


@property (weak, nonatomic) IBOutlet UIScrollView *containerScrollView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UIImageView *foodImageView;
@property (weak, nonatomic) IBOutlet UILabel *shopNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (weak, nonatomic) IBOutlet HCSStarRatingView *foodRatingView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *priceRatingView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *serviceRatingView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *decoreRatingView;


@property (weak, nonatomic) IBOutlet UIView *mapView;
@property (weak, nonatomic) IBOutlet UIView *reviewView;
@property (weak, nonatomic) IBOutlet UIView *loveView;
@property (weak, nonatomic) IBOutlet UIView *shareView;

@property (weak, nonatomic) IBOutlet UIButton *mapBtn;
@property (weak, nonatomic) IBOutlet UIButton *reviewBtn;
@property (weak, nonatomic) IBOutlet UIButton *loveBtn;
@property (weak, nonatomic) IBOutlet UIButton *shareBtn;

- (IBAction)mapBtnTap:(id)sender;
- (IBAction)reviewBtnTap:(id)sender;
- (IBAction)loveBtnTap:(id)sender;
- (IBAction)shareBtnTap:(id)sender;

@property (weak, nonatomic) IBOutlet MKMapView *loactionMapView;
@property (weak, nonatomic) IBOutlet UITableView *reviewTableView;

@property (weak, nonatomic) IBOutlet UILabel *openTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *offDayLabel;


@end

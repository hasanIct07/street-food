//
//  CalloutView.h
//  Street Food
//
//  Created by Mehedi Hasan on 2/10/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol CalloutViewDelegate <NSObject>

-(void)didTapSeeDetailsBtnInCalloutView:(id)sender;
-(void)didTapMapBtnInCalloutView:(id)sender;
-(void)didTapCloseBtnInCalloutView:(id)sender;
@end

@interface CalloutView : UIView

+ (CalloutView *)viewFromNib;

@property (strong, nonatomic) id<CalloutViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *foodNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *shopNameLabel;

- (IBAction)seeDetailsBtnTap:(id)sender;
- (IBAction)mapBtnTap:(id)sender;
- (IBAction)closeBtnTap:(id)sender;


@end

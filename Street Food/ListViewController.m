//
//  ListViewController.m
//  Street Food
//
//  Created by Mehedi Hasan on 1/14/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import "ListViewController.h"
#import "FoodCell.h"
#import "DetailsViewController.h"
#import "APIManager.h"
#import "Food.h"
#import "LocationService.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Constants.h"

@interface ListViewController (){
    
    NSMutableArray *foodsAry;
    
    //Searchable
    NSString *searchString;
    BOOL isSearched;
    
    int offset;
    int size;
    int currentPageNumber;
    
    BOOL isPageRefresing;
    BOOL shouldPageRefresh;
    
    
}

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    foodsAry = [NSMutableArray new];
    shouldPageRefresh = YES;
    offset = 0;
    size = 20;
    currentPageNumber = 0;
    
    isPageRefresing = YES;
    [self getFoodsWithQuery:nil];
    
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    singleFingerTap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:singleFingerTap];
    [self.navigationController.view addGestureRecognizer:singleFingerTap];
}

- (void)viewTapped:(UITapGestureRecognizer *)recognizer {

    [self.view endEditing:YES];
}

- (void)getFoodsWithQuery:(NSString *)queryStr{
    
    //page
    currentPageNumber++;
    
    NSMutableDictionary *foodParams = [NSMutableDictionary new];
    foodParams[@"size"] = @(size);
    foodParams[@"offset"] = @(offset);
    
    if(queryStr != nil){
        
        foodParams[@"foodName"] = queryStr;
    }
    
    Food *food = [[Food alloc] init];
    food.foodId = @(1);
    food.name = @"Doi Fuchka";
    food.price =@"5";
    food.imageUrl =@"/Uploads/636211666070897098_photo.jpg";
    food.shopName = @"Bangla Fuschka";
    food.shopAddress = @"Shahbagh, Dhaka, Bangladesh";
    food.latitude = 23.7397263f;
    food.longitude =  90.394261f;
    food.shopOpeningTime = @"09:00 AM";
    food.shopClosingTime = @"09:00 PM";
    
    food.averageRating = 4.5;
    food.averageFoodRating =  4;
    food.averagePriceRating = 5;
    food.averageServiceRating = 4.4;
    food.averageDecoreRating = 4.5;
    food.favoriteCount = 10;
    
    Food *food1 = [[Food alloc] init];
    food1.foodId = @(1);
    food1.name = @"Noodles";
    food1.price =@"5";
    food1.imageUrl =@"/Uploads/636211845389373327_photo.jpg";
    food1.shopName = @"Bangla Noodles";
    food1.shopAddress = @"Nilkhet, Dhaka, Bangladesh";
    food1.latitude = 23.732815f;
    food1.longitude =  90.395752f;
    food1.shopOpeningTime = @"09:00 AM";
    food1.shopClosingTime = @"09:00 PM";
    
    food1.averageRating = 4.5;
    food1.averageFoodRating =  4;
    food1.averagePriceRating = 5;
    food1.averageServiceRating = 4.4;
    food1.averageDecoreRating = 4.5;
    food1.favoriteCount = 6;
    
    isPageRefresing = YES;
    shouldPageRefresh = NO;
    [foodsAry addObject:food];
    [foodsAry addObject:food1];
    [self.foodTableView reloadData];
    return;
    
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        isPageRefresing = NO;
//    })
    
    
    [[APIManager sharedManager] getFoodWithParam:foodParams Completion:^(NSArray *foods, BOOL success) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            isPageRefresing = NO;
        });

        if(success && foods && foods.count > 0){
            
            if(foods.count < size){
                shouldPageRefresh = NO;
            }
            
            [foodsAry addObjectsFromArray:foods];
            

            
            offset = offset + (int)foods.count;

            [self.foodTableView reloadData];
        } else {
            shouldPageRefresh = NO;
        }
    }];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    DetailsViewController *vc = (DetailsViewController  *)segue.destinationViewController;
    vc.hidesBottomBarWhenPushed = YES;
    
    if ([segue.identifier isEqualToString:@"ShowFood"])
    {
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        Food *food = (Food *)[foodsAry objectAtIndex:indexPath.row];
        vc.selectedFood = food;
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    shouldPageRefresh = YES;
    
    NSLog(@"ListView: viewWillAppear");
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSLog(@"ListView: viewDidAppear");
}

-(void)viewWillDisappear:(BOOL)animated{
    
    NSLog(@"ListView: viewWillDisappear");
    
    [super viewWillDisappear:animated];
}

-(void)viewDidDisappear:(BOOL)animated{
    
    NSLog(@"ListView: viewDidDisappear");
    
    [super viewWillDisappear:animated];
}

#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //return 6;
    return foodsAry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"FoodCell1";
    
    FoodCell *cell = (FoodCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    //FoodCell * cell = (FoodCell *) [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"FoodCell%d", (indexPath.row+1)]];
    Food *food = (Food *)[foodsAry objectAtIndex:indexPath.row];
    
    cell.foodNameLabel.text = food.name;
    cell.favoriteLabel.text = [NSString stringWithFormat:@"%d Love it", food.favoriteCount];
    cell.distanceLabel.text = [[LocationService sharedInstance] getDistanceFromLat:food.latitude AndLong:food.longitude];
    cell.averageRatingView.value = food.averageRating;
    
    if(food.imageUrl != nil){
        
        NSString * urlStr = [NSString stringWithFormat:@"%@%@",BaseURLString, food.imageUrl];
        [cell.foodImageView sd_setImageWithURL:[NSURL URLWithString:urlStr]
                     placeholderImage:[UIImage imageNamed:@"photo"]];
    }else{
        
        cell.foodImageView.image = [UIImage imageNamed:@"photo"];
    }
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.searchBar isFirstResponder]){
        [self.searchBar resignFirstResponder];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    
    
    [self performSegueWithIdentifier:@"ShowFood" sender:indexPath];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if(self.foodTableView.contentOffset.y >= (self.foodTableView.contentSize.height - self.foodTableView.bounds.size.height)) {
        if(shouldPageRefresh){
            if(isPageRefresing == NO){
                isPageRefresing = YES;
                
                //[self getFoodsWithQuery:nil];
            }
        }
    }
}


#pragma mark - UITableViewDataSource


- (void)didReceiveMemoryWarning {
    NSLog(@"didReceiveMemoryWarning");
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UISearchBarDelegate

//- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar;                      // return NO to not become first responder
//- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar;                     // called when text starts editing
//- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar;                        // return NO to not resign first responder
//- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar;                       // called when text ends editing
//- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;   // called when text changes (including clear)
//- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    searchString = searchBar.text;
    
    [foodsAry removeAllObjects];
    [self.foodTableView reloadData];
    
    shouldPageRefresh = YES;
    offset = 0;
    size = 20;
    currentPageNumber = 0;
    
    isPageRefresing = YES;
    
    //[self getFoodsWithQuery:searchBar.text];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if ([searchText length] == 0) {
        
        searchString = nil;
        
        [foodsAry removeAllObjects];
        [self.foodTableView reloadData];
        
        shouldPageRefresh = YES;
        offset = 0;
        size = 20;
        currentPageNumber = 0;
        
        isPageRefresing = YES;
        
        //[self getFoodsWithQuery:nil];
    }
}
@end

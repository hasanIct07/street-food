//
//  CalloutView.m
//  Street Food
//
//  Created by Mehedi Hasan on 2/10/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import "CalloutView.h"

@implementation CalloutView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

// MARK: - Actions

- (IBAction)seeDetailsBtnTap:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(didTapSeeDetailsBtnInCalloutView:)]) {
        [self.delegate didTapSeeDetailsBtnInCalloutView:self];
    }
}

- (IBAction)mapBtnTap:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(didTapMapBtnInCalloutView:)]) {
        [self.delegate didTapMapBtnInCalloutView:self];
    }
}

- (IBAction)closeBtnTap:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(didTapCloseBtnInCalloutView:)]) {
        [self.delegate didTapCloseBtnInCalloutView:self];
    }
}

#pragma mark - UIView

+ (CalloutView *)viewFromNib {
    
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"CalloutView"
                                                      owner:self
                                                    options:nil];
    
    if(nibViews.count > 0){
        
        CalloutView * calloutView = (CalloutView *)[nibViews firstObject];
        calloutView.translatesAutoresizingMaskIntoConstraints = NO;
        return calloutView;
    }
    
    return nil;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    
    self.imageView.layer.cornerRadius = self.imageView.bounds.size.height / 2.0;
}

@end

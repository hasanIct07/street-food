//
//  FoodAnnotation.m
//  Street Food
//
//  Created by Mehedi Hasan on 2/10/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import "FoodAnnotation.h"

@implementation FoodAnnotation

- (instancetype)initwithFood:(Food*)food{
    
    if (self == [super init]) {
        
        self.food = food;
    }
    return self;
}
- (CLLocationCoordinate2D)coordinate
{
    CLLocationCoordinate2D theCoordinate;
    theCoordinate.latitude = _food.latitude;
    theCoordinate.longitude = _food.longitude;
    return theCoordinate;
}

@end

//
//  AddViewController.m
//  Street Food
//
//  Created by Mehedi Hasan on 1/14/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import "AddViewController.h"
#import "LocationService.h"
#import "APIManager.h"
#import "HelperClass.h"
//#import "Constants.h"

typedef enum ActiveField : NSUInteger {
    kActiveTextField,
    kActiveTextView
} ActiveField;

@interface AddViewController () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate>

@end

@implementation AddViewController {
    
    UIImagePickerController *_imagePicker;
    UIActionSheet *_actionSheet;
    UIImage *_image;
    
    ActiveField activeField;
    UITextField *activeTextField;
    UITextView * activeTextView;
    
    MKPointAnnotation *foodPoint;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationItem setTitle:@"Add Food"];
    
    foodPoint = [[MKPointAnnotation alloc] init];
    
    [self setupView];
    [self setupAddressSearchTF];
    
    self.scrollView.delegate = self;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mapViewTapped:)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.numberOfTouchesRequired = 1;
    [self.mapView addGestureRecognizer:tapRecognizer];
    
    //[self.openTimeTextField addTarget:self action:@selector(openTimeTouched:) forControlEvents:UIControlEventEditingDidBegin];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    NSLog(@"AddView: viewWillAppear");
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self setupAddressSearchTFOptionalProperties];
    NSLog(@"AddView: viewDidAppear");
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    NSLog(@"AddView: viewWillDisappear");
    
    [super viewWillDisappear:animated];
}

-(void)viewDidDisappear:(BOOL)animated{
    
    NSLog(@"AddView: viewDidDisappear");
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    NSLog(@"didReceiveMemoryWarning");
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setupView {
    
    self.addFoodBtn.layer.cornerRadius = 5.0f;
    
    self.descriptionTV.placeholder=@"Enter description here";
    UIColor *placeHolderColor = [UIColor colorWithRed: 199/255.f green: 199/255.f blue:205/255.f alpha:1];
    [self.descriptionTV setPlaceholderColor: placeHolderColor];
    [self.descriptionTV setPlaceholderFont:[UIFont systemFontOfSize:16.0f weight:UIFontWeightRegular]];
    
    
    //Map
    self.selectedFoodCoordinate = [[LocationService sharedInstance] getMyCoordinate];
    MKCoordinateSpan span = MKCoordinateSpanMake(0.05, 0.05);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMake(self.selectedFoodCoordinate, span)];
    [self.mapView setRegion:adjustedRegion animated:true];
}

#pragma mark - Address Location

-(void)setupAddressSearchTF {
    
    _addressTF.placeSearchDelegate                 = self;
    _addressTF.strApiKey                           = @"AIzaSyDtbaFrXi8c7QXZlLvra3BWFFIsCHi2El0";
    _addressTF.superViewOfList                     = self.view;  // View, on which Autocompletion list should be appeared.
    _addressTF.autoCompleteShouldHideOnSelection   = YES;
    _addressTF.maximumNumberOfAutoCompleteRows     = 5;
}

-(void)setupAddressSearchTFOptionalProperties{
    
    //Optional Properties
    _addressTF.autoCompleteRegularFontName =  @"HelveticaNeue-Bold";
    _addressTF.autoCompleteBoldFontName = @"HelveticaNeue";
    _addressTF.autoCompleteTableCornerRadius = 0.0;
    _addressTF.autoCompleteRowHeight = 35;
    _addressTF.autoCompleteTableCellTextColor = [UIColor colorWithWhite:0.131 alpha:1.000];
    _addressTF.autoCompleteFontSize = 14;
    _addressTF.autoCompleteTableBorderWidth = 1.0;
    _addressTF.showTextFieldDropShadowWhenAutoCompleteTableIsOpen = YES;
    _addressTF.autoCompleteShouldHideOnSelection=YES;
    _addressTF.autoCompleteShouldHideClosingKeyboard = YES;
    _addressTF.autoCompleteShouldSelectOnExactMatchAutomatically = YES;
    
    _addressTF.autoCompleteTableFrame = CGRectMake((self.addressView.frame.origin.x-2), (74 + self.addressView.frame.size.height), (self.addressView.frame.size.width + 4), 200.0);
}

#pragma mark - UIKeyboard Notification
- (void)keyboardWasShown:(NSNotification*)aNotification {
    
    //NSDictionary* info = [aNotification userInfo];
    //CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    if(activeField == kActiveTextField && activeTextField == self.addressTF ){
        
        //CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
        [self.scrollView setContentOffset:CGPointMake(0, 98) animated:YES];
        
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    
    
    //UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    //self.scrollView.contentInset = contentInsets;
    //self.scrollView.scrollIndicatorInsets = contentInsets;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBAction

- (IBAction)addFoodBtnTapped:(id)sender {
    
    [self.view endEditing:YES];
    
    NSString* name = [NSString stringWithString:self.foodNameTF.text];
    NSString* price = [NSString stringWithString:self.priceTF.text];
    NSString* shopName = [NSString stringWithString:self.shopNameTF.text];
    NSString* address = [NSString stringWithString:self.addressTF.text];
    
    NSString* openingTime = [NSString stringWithString:self.openTimeTF.text];
    NSString* closingTime = [NSString stringWithString:self.closeTimeTF.text];
    NSString* description = [NSString stringWithString:self.descriptionTV.text];
    
    
    
    NSString* latitude = [NSString stringWithFormat:@"%.8f",self.selectedFoodCoordinate.latitude];
    NSString* longitude = [NSString stringWithFormat:@"%.8f",self.selectedFoodCoordinate.longitude];
    
    
//    if( [allTrim(name) length] == 0 || [allTrim(price) length] == 0 ||[allTrim(shopName) length] == 0 || [allTrim(address) length] == 0 || [allTrim(openingTime) length] == 0 || [allTrim(closingTime) length] == 0 || [allTrim(description) length] == 0) {
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"You must input all information" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
//        
//        return;
//    }
    
    
    
    if(self.selectedFoodCoordinate.latitude == 0.0f && self.selectedFoodCoordinate.longitude == 0.0f){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Invalid food Loation." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
        self.addressTF.text = @"";
        return;
    }
    
    if(_image == nil ){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Please upload image." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    
    
    NSDictionary *foodParams = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"1", @"UserId",
                            name, @"FoodName",
                            price, @"Price",
                            shopName, @"ShopName",
                            address, @"ShopAddress",
                            latitude, @"Latitude",
                            longitude, @"Longitude",
                            openingTime, @"ShopOpeningTime",
                            closingTime, @"ShopClosingTime",
                            description, @"FoodDescription", nil];
    
    
    [[APIManager sharedManager] postFood:foodParams withPhoto:_image Completion:^(NSString *message, BOOL success) {
        
        if(success){
            
            self.foodNameTF.text = @"";
            self.priceTF.text = @"";
            self.shopNameTF.text = @"";
            self.addressTF.text = @"";
            self.openTimeTF.text = @"";
            self.closeTimeTF.text = @"";
            self.descriptionTV.text = @"";

            
            self.selectedFoodCoordinate = CLLocationCoordinate2DMake(0.0f, 0.0f);
            self.selectedOpeningTime = [NSDate date];
            self.selectedClosingTime = [NSDate date];
            _image = nil;
            self.uploadImageView.image = [UIImage imageNamed:@"photoGray"];
        }
    }];
}


- (IBAction)addPhotoBtnTapped:(id)sender {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        _actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:@"Take Photo", @"Choose From Library", nil];
        
        [_actionSheet showInView:self.view];
        
    } else {
        
        [self choosePhotoFromLibrary];
    }
}

-(IBAction)mapViewTapped:(UITapGestureRecognizer *)recognizer {
    
    CGPoint point = [recognizer locationInView:self.mapView];
    CLLocationCoordinate2D tapPoint = [self.mapView convertPoint:point toCoordinateFromView:self.mapView];
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    MKPointAnnotation *point1 = [[MKPointAnnotation alloc] init];
    point1.coordinate = tapPoint;
    
    point1.title = @"Food Location";
    point1.subtitle = @"";
    
    [self.mapView addAnnotation:point1];
    
    [[HelperClass sharedInstance] showHud];
    
    [[APIManager sharedManager] getLocationAddressOfLat:tapPoint.latitude Long:tapPoint.longitude Completion:^(NSString *address, BOOL success) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //update UI in main thread.
            [[HelperClass sharedInstance] hideHUd];
            if (success) {
                self.addressTF.text = address;
                self.selectedFoodCoordinate = tapPoint;
            } else {
                [[HelperClass sharedInstance] showErrorMesssege:@"No place found!"];
                self.addressTF.text = @"";
                self.selectedFoodCoordinate = CLLocationCoordinate2DMake(0.0f, 0.0f);
                [self.mapView removeAnnotations:self.mapView.annotations];
            }
        });
    }];
}


- (void)takePhoto
{
    _imagePicker = [[UIImagePickerController alloc] init];
    _imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    _imagePicker.delegate = self;
    _imagePicker.allowsEditing = YES;
    [self presentViewController:_imagePicker animated:YES completion:nil];
}



- (void)choosePhotoFromLibrary
{
    _imagePicker = [[UIImagePickerController alloc] init];
    _imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    _imagePicker.delegate = self;
    _imagePicker.allowsEditing = YES;
    [self presentViewController:_imagePicker animated:YES completion:nil];
}


#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        [self takePhoto];
        
    } else if (buttonIndex == 1) {
        
        [self choosePhotoFromLibrary];
    }
    
    _actionSheet = nil;
}

#pragma mark - UIImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSLog(@"Picker returned successfully.");
    
    UIImage *selectedImage;
    NSURL *mediaUrl = (NSURL *)[info valueForKey:UIImagePickerControllerMediaURL];
    
    if (mediaUrl == nil) {
        
        selectedImage = (UIImage *) [info valueForKey:UIImagePickerControllerEditedImage];
        if (selectedImage == nil) {
            
            selectedImage= (UIImage *) [info valueForKey:UIImagePickerControllerOriginalImage];
            NSLog(@"Original image picked.");
            
        } else {
            NSLog(@"Edited image picked.");
        }
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    _image = selectedImage;
    self.uploadImageView.image = selectedImage;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Place search Textfield Delegates

-(void)placeSearch:(MVPlaceSearchTextField*)textField ResponseForSelectedPlace:(GMSPlace*)responseDict{
    [self.view endEditing:YES];
    
    self.selectedFoodCoordinate = responseDict.coordinate;
    //Str_Latitude = [NSString stringWithFormat:@"%.8f",self.currennCoordinate.latitude];
    //Str_Longitude = [NSString stringWithFormat:@"%.8f",self.currennCoordinate.longitude];
    
    NSLog(@"SELECTED ADDRESS :%@",responseDict);
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    foodPoint.coordinate = self.selectedFoodCoordinate;
    foodPoint.title = @"Food Location";
    foodPoint.subtitle = @"";
    [self.mapView addAnnotation:foodPoint];
    
    [self.mapView setCenterCoordinate:self.selectedFoodCoordinate animated:YES];
}

-(void)placeSearchWillShowResult:(MVPlaceSearchTextField*)textField{
    
}
-(void)placeSearchWillHideResult:(MVPlaceSearchTextField*)textField{
    
}
-(void)placeSearch:(MVPlaceSearchTextField*)textField ResultCell:(UITableViewCell*)cell withPlaceObject:(PlaceObject*)placeObject atIndex:(NSInteger)index{
    if(index%2==0){
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField == self.openTimeTF || textField == self.closeTimeTF){

        return NO;
    }
    
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    activeField = kActiveTextField;
    activeTextField = textField;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    activeTextField = nil;
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    [textField resignFirstResponder];
    
    if (textField == self.foodNameTF) {
        
        [self.priceTF becomeFirstResponder];
        
    } else if (textField == self.priceTF) {
        
        [self.shopNameTF becomeFirstResponder];
    }else if (textField == self.shopNameTF) {
    
        [self.addressTF becomeFirstResponder];
    }else if (textField == self.addressTF) {
        
        //[self. becomeFirstResponder];
        
    } else if (textField == self.priceTF) {
        
        [self.descriptionTV becomeFirstResponder];
    }
    
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return textField.text.length + (string.length - range.length) <= 70;
}


#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    return textView.text.length + (text.length - range.length) <= 151;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    NSLog(@"Did begin editing");
    activeField = kActiveTextView;
    activeTextView = textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    //[textView resignFirstResponder];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
 
    //NSLog(@"%f", scrollView.contentOffset.y);
}

#pragma mark - Time Select

- (IBAction)openTimeTouched:(id)sender {
    
    UITextField *selectedTextField = (UITextField *) sender;
    
    if (selectedTextField.tag == 60){
        //Open Hour
        
        NSInteger minuteInterval = 5;
        //clamp date
        NSInteger referenceTimeInterval = (NSInteger)[self.selectedOpeningTime timeIntervalSinceReferenceDate];
        NSInteger remainingSeconds = referenceTimeInterval % (minuteInterval *60);
        NSInteger timeRoundedTo5Minutes = referenceTimeInterval - remainingSeconds;
        if(remainingSeconds>((minuteInterval*60)/2)) {/// round up
            timeRoundedTo5Minutes = referenceTimeInterval +((minuteInterval*60)-remainingSeconds);
        }
        
        self.selectedOpeningTime = [NSDate dateWithTimeIntervalSinceReferenceDate:(NSTimeInterval)timeRoundedTo5Minutes];
        
        ActionSheetDatePicker *datePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select a time" datePickerMode:UIDatePickerModeTime selectedDate:self.selectedOpeningTime target:self action:@selector(timeWasSelected:element:) origin:sender];
        datePicker.minuteInterval = minuteInterval;
        [datePicker showActionSheetPicker];
        
    } else {
        //Close Hour
        
        NSInteger minuteInterval = 5;
        //clamp date
        NSInteger referenceTimeInterval = (NSInteger)[self.selectedClosingTime timeIntervalSinceReferenceDate];
        NSInteger remainingSeconds = referenceTimeInterval % (minuteInterval *60);
        NSInteger timeRoundedTo5Minutes = referenceTimeInterval - remainingSeconds;
        if(remainingSeconds>((minuteInterval*60)/2)) {/// round up
            timeRoundedTo5Minutes = referenceTimeInterval +((minuteInterval*60)-remainingSeconds);
        }
        
        self.selectedClosingTime = [NSDate dateWithTimeIntervalSinceReferenceDate:(NSTimeInterval)timeRoundedTo5Minutes];
        
        ActionSheetDatePicker *datePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select a time" datePickerMode:UIDatePickerModeTime selectedDate:self.selectedClosingTime target:self action:@selector(timeWasSelected:element:) origin:sender];
        datePicker.minuteInterval = minuteInterval;
        [datePicker showActionSheetPicker];
        
    }
    

}

-(void)timeWasSelected:(NSDate *)selectedTime element:(id)element {
    
    UITextField *selectedTextField = (UITextField *) element;
    
    if (selectedTextField.tag == 60){
        //Open Hour
        self.selectedOpeningTime = selectedTime;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"h:mm a"];
        self.openTimeTF.text = [dateFormatter stringFromDate:selectedTime];
        
    } else {
        
        self.selectedClosingTime = selectedTime;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"h:mm a"];
        self.closeTimeTF.text = [dateFormatter stringFromDate:selectedTime];
    }
}


@end

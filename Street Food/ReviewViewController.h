//
//  ReviewViewController.h
//  Street Food
//
//  Created by Mehedi Hasan on 1/21/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Food.h"
#import "HCSStarRatingView.h"

@interface ReviewViewController : UIViewController

@property (nonatomic, strong) Food *selectedFood;

@property (weak, nonatomic) IBOutlet HCSStarRatingView *foodRatingView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *serviceRatingView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *decoreRatingView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *priceRatingView;

@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTV;


- (IBAction)cancelBtnTap:(id)sender;
- (IBAction)submitBtnTap:(id)sender;

@end

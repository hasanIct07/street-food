//
//  MapViewController.h
//  Street Food
//
//  Created by Mehedi Hasan on 1/14/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *foodMapView;

@end

//
//  AnnotationView.m
//  Street Food
//
//  Created by Mehedi Hasan on 2/10/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import "AnnotationView.h"

static NSString * annotationID = @"AnnotationID";

@interface AnnotationView ()

@property (nonatomic,retain) AnnotationView *contentView;

@end


@implementation AnnotationView


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
#pragma mark - MKAnnotationView

- (instancetype)initWithAnnotation:(nullable id <MKAnnotation>)annotation reuseIdentifier:(nullable NSString *)reuseIdentifier {
    
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    
    if(self){
        
        self.contentView = [AnnotationView viewFromNib];
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.contentView.frame.size.width, self.contentView.frame.size.height);
        [self addSubview:self.contentView];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    
    if(self = [super initWithFrame:frame]){
        
    }
    
    return self;
}

#pragma mark - Setter & Getter

-(UILabel *)label{
    
    return self.contentView.contentLabel;
}

-(UIImageView *)imageView{
    
    return self.contentView.contentImageView;
}

-(void)setCalloutView:(UIView *)calloutView{
    
    if(_calloutView != nil){
        [_calloutView removeFromSuperview];
        _calloutView = nil;
    }
    _calloutView = calloutView;

    
    if(_calloutView != nil){
        
        [self addSubview:calloutView];
        
        NSLayoutConstraint *constraintVertical = [NSLayoutConstraint constraintWithItem:self
                                                                              attribute:NSLayoutAttributeCenterX
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:_calloutView
                                                                              attribute:NSLayoutAttributeCenterX
                                                                             multiplier:1.0f
                                                                               constant:0.0f];
        
        NSLayoutConstraint *constraintBottom = [NSLayoutConstraint constraintWithItem:self
                                                                            attribute:NSLayoutAttributeTop
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:_calloutView
                                                                            attribute:NSLayoutAttributeBottom
                                                                           multiplier:1.0f
                                                                             constant:0.0f];
        [self addConstraint:constraintVertical];
        [self addConstraint:constraintBottom];
    }
    

}


#pragma mark - UIView

-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    if(self.calloutView && CGRectContainsPoint(self.calloutView.frame, point)) {
        
        return [self.calloutView hitTest:[self.calloutView convertPoint:point fromView:self] withEvent:event];
    }
    
    if ( CGRectContainsPoint(CGRectMake(0.0, 0.0, self.bounds.size.width, self.bounds.size.height / 2.0), point) ) {
        
        return [super hitTest:point withEvent:event];
    }
    
    return nil;
}

+ (AnnotationView *)viewFromNib {
    
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"AnnotationView"
                                                      owner:self
                                                    options:nil];
    
    if(nibViews.count > 0){
        
        return  (AnnotationView *)[nibViews firstObject];
    }
    
    return nil;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    
    self.imageView.layer.cornerRadius = self.imageView.bounds.size.height / 2.0;
}

@end

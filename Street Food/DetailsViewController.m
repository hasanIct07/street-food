//
//  DetailsViewController.m
//  Street Food
//
//  Created by Mehedi Hasan on 1/21/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import "DetailsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Constants.h"
#import "ReviewViewController.h"
#import "LocationService.h"

@interface DetailsViewController ()<KIImagePagerDelegate, KIImagePagerDataSource>

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationItem setTitle:@"Food Details"];
    
    self.containerScrollView.delaysContentTouches = YES;
    self.containerScrollView.canCancelContentTouches = NO;
    

    UITapGestureRecognizer *tapMap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mapBtnTap:)];
    [self.mapView addGestureRecognizer:tapMap];

    UITapGestureRecognizer *tapReview = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reviewBtnTap:)];
    [self.reviewView addGestureRecognizer:tapReview];
    
    UITapGestureRecognizer *tapLove = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loveBtnTap:)];
    [self.loveView addGestureRecognizer:tapLove];
    
    UITapGestureRecognizer *tapShare = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareBtnTap:)];
    [self.shareView addGestureRecognizer:tapShare];
    
    [self setupView];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //self.foodImageView.image = [UIImage imageNamed:self.foodImageName];
    
    self.title = self.selectedFood.name;
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc]
                                initWithTitle:@""
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:nil];
    self.navigationController.navigationBar.topItem.backBarButtonItem = btnBack;
}

-(void)setupView {
    
    if(self.selectedFood.imageUrl != nil){
        
        NSString * urlStr = [NSString stringWithFormat:@"%@%@",BaseURLString, self.selectedFood.imageUrl];
        [self.foodImageView sd_setImageWithURL:[NSURL URLWithString:urlStr]
                              placeholderImage:[UIImage imageNamed:@"photo"]];
    }else{
        
        self.foodImageView.image = [UIImage imageNamed:@"photo"];
    }
    
    self.shopNameLabel.text = self.selectedFood.shopName;
    self.addressLabel.text = self.selectedFood.shopAddress;
    
    self.priceRatingView.value = self.selectedFood.averagePriceRating;
    self.foodRatingView.value = self.selectedFood.averageFoodRating;
    self.serviceRatingView.value = self.selectedFood.averageServiceRating;
    self.decoreRatingView.value = self.selectedFood.averageDecoreRating;
    
    self.openTimeLabel.text = [NSString stringWithFormat:@"OPEN %@ - %@", self.selectedFood.shopOpeningTime, self.selectedFood.shopClosingTime];
    
    self.distanceLabel.text = [[LocationService sharedInstance] getDistanceFromLat: self.selectedFood.latitude AndLong: self.selectedFood.longitude];
    
    // Add an annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    self.shopCoordinate = CLLocationCoordinate2DMake(self.selectedFood.latitude, self.selectedFood.longitude);
    point.coordinate = self.shopCoordinate;
    point.title = @"Shop Address";
    point.subtitle = @"";
    
    [self.loactionMapView addAnnotation:point];

    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.shopCoordinate, 800, 800);
    [self.loactionMapView setRegion:[self.loactionMapView regionThatFits:region] animated:YES];
}

#pragma mark - KIImagePager DataSource
- (NSArray *) arrayWithImages:(KIImagePager*)pager
{
    
    return self.foodImageAry;
    
    //    return @[
    //             @"https://raw.github.com/kimar/tapebooth/master/Screenshots/Screen1.png",
    //             @"https://raw.github.com/kimar/tapebooth/master/Screenshots/Screen2.png",
    //             @"https://raw.github.com/kimar/tapebooth/master/Screenshots/Screen3.png"
    //             ];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    
    if([segue.identifier isEqualToString:@"AddReview"]){
        ReviewViewController *vc = (ReviewViewController *)segue.destinationViewController;
        //vc.hidesBottomBarWhenPushed = YES;
        vc.selectedFood = self.selectedFood;
    }
}


- (IBAction)mapBtnTap:(id)sender {
    
    [self.loactionMapView setCenterCoordinate:self.shopCoordinate animated:YES];
}

- (IBAction)reviewBtnTap:(id)sender {
}

- (IBAction)loveBtnTap:(id)sender {
    
    if (self.loveBtn.tag == 0){
        
        self.loveBtn.tag = 1;
        [self.loveBtn setImage:[UIImage imageNamed:@"heartFilled"] forState:UIControlStateNormal];
    } else{
        
        self.loveBtn.tag = 0;
        [self.loveBtn setImage:[UIImage imageNamed:@"heartGray"] forState:UIControlStateNormal];
    }
}

- (IBAction)shareBtnTap:(id)sender {
    
}
@end

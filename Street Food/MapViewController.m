//
//  MapViewController.m
//  Street Food
//
//  Created by Mehedi Hasan on 1/14/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import "MapViewController.h"
#import "APIManager.h"
#import "LocationService.h"
#import "AnnotationView.h"
#import "FoodAnnotation.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Constants.h"
#import "CalloutView.h"
#import "DetailsViewController.h"
#import "Street_Food-Swift.h"


@class MapManager;

@interface MapViewController () <CalloutViewDelegate, GoogleAPIsDelegate>{
    
    MKCoordinateSpan userCoordinateSpan;
    MapManager *mapManager;
    
    //MapManager *mapManager;
    
    AnnotationView* selectedAnnotationView;
    FoodAnnotation* selectedFoodAnnotation;
    MKPolyline *drawnPolyLine;
}

@property CalloutView *calloutView;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationItem setTitle:@"Street Food"];
    userCoordinateSpan = MKCoordinateSpanMake(0.05, 0.05);
    
    self.calloutView = [CalloutView viewFromNib];
    self.calloutView.delegate = self;
    mapManager = [[MapManager alloc] init];
    mapManager.delegate = self;
    
    [self setupView];
    [self getFoodsFromAPI];
}

- (void)getFoodsFromAPI {
    
    Food *food = [[Food alloc] init];
    food.foodId = @(8);
    food.name = @"Doi Fuchka";
    food.price =@"5";
    food.imageUrl =@"/Uploads/636211666070897098_photo.jpg";
    food.shopName = @"Bangla Fuschka";
    food.shopAddress = @"Shahbagh, Dhaka, Bangladesh";
    food.latitude = 23.7397263f;
    food.longitude =  90.394261f;
    food.shopOpeningTime = @"09:00 AM";
    food.shopClosingTime = @"09:00 PM";
    
    food.averageRating = 4.5;
    food.averageFoodRating =  4;
    food.averagePriceRating = 5;
    food.averageServiceRating = 4.4;
    food.averageDecoreRating = 4.5;
    food.favoriteCount = 10;
    
    Food *food1 = [[Food alloc] init];
    food1.foodId = @(8);
    food1.name = @"Noodles";
    food1.price =@"5";
    food1.imageUrl =@"/Uploads/636211845389373327_photo.jpg";
    food1.shopName = @"Bangla Noodles";
    food1.shopAddress = @"Nilkhet, Dhaka, Bangladesh";
    food1.latitude = 23.732815f;
    food1.longitude =  90.395752f;
    food1.shopOpeningTime = @"09:00 AM";
    food1.shopClosingTime = @"09:00 PM";
    
    food1.averageRating = 4.5;
    food1.averageFoodRating =  4;
    food1.averagePriceRating = 5;
    food1.averageServiceRating = 4.4;
    food1.averageDecoreRating = 4.5;
    food1.favoriteCount = 6;
    
    NSMutableArray *foods = [NSMutableArray new];
    [foods addObject:food];
    [foods addObject:food1];
    
    
    
    NSMutableArray *annotationArray = [NSMutableArray new];
    
    for (Food * food in foods) {
        
        FoodAnnotation *annotation = [[FoodAnnotation alloc] initwithFood:food];
        [annotationArray addObject:annotation];
    }
    
    [self.foodMapView addAnnotations:annotationArray];
    return;
    
    NSMutableDictionary *foodParams = [NSMutableDictionary new];
    foodParams[@"size"] = @(50);
    foodParams[@"offset"] = @(0);
    
    
    [[APIManager sharedManager] getFoodWithParam:foodParams Completion:^(NSArray *foods, BOOL success) {
        
        if(success && foods && foods.count > 0){
            
            NSMutableArray *annotationArray = [NSMutableArray new];
            
            for (Food * food in foods) {
                
                FoodAnnotation *annotation = [[FoodAnnotation alloc] initwithFood:food];
                [annotationArray addObject:annotation];
            }

            [self.foodMapView addAnnotations:annotationArray];
        }
    }];
}


-(void)setupView {
    
    self.foodMapView.delegate =  self;
    self.foodMapView.showsUserLocation = YES;
    
    //UITapGestureRecognizer* tapRec = [[UITapGestureRecognizer alloc]
                                      //initWithTarget:self action:@selector(didTapMap:)];
    //[self.foodMapView addGestureRecognizer:tapRec];
    
    //MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.shopCoordinate, 800, 800);
    [self.foodMapView setRegion:MKCoordinateRegionMake([[LocationService sharedInstance] getMyCoordinate], userCoordinateSpan) animated:YES];
}

-(void)didTapMap:(UIGestureRecognizer *)gesture{
    
    //[self.foodMapView deselectAnnotation:selectedAnnotationView animated:YES];
}


- (void)didReceiveMemoryWarning {
    NSLog(@"didReceiveMemoryWarning");
    [super didReceiveMemoryWarning];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"MapView: viewWillAppear");
    
    [self removeAllPlacemarkFromMap];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSLog(@"MapView: viewDidAppear");
}
-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"MapView: viewWillDisappear");
    [super viewWillDisappear:animated];
}
-(void)viewDidDisappear:(BOOL)animated{
    NSLog(@"MapView: viewDidDisappear");
    [super viewWillDisappear:animated];
}



#pragma mark - CalloutViewDelegate

-(void)didTapMapBtnInCalloutView:(id)sender{

    
    
    [self didTapCloseBtnInCalloutView:self];
    
    CLLocationCoordinate2D selfCoordinate = [[LocationService sharedInstance] getMyCoordinate];
    [mapManager directionsUsingGoogleFrom:selfCoordinate to:selectedFoodAnnotation.coordinate];
 
    /*
    double latitude = selectedFoodAnnotation.coordinate.latitude;
    double longitude = selectedFoodAnnotation.coordinate.longitude;
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(latitude, longitude) addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    [request setSource:[MKMapItem mapItemForCurrentLocation]];
    [request setDestination:mapItem];
    [request setTransportType:MKDirectionsTransportTypeAny]; // This can be limited to automobile and walking directions.
    [request setRequestsAlternateRoutes:YES]; // Gives you several route options.
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        if (!error) {
            for (MKRoute *route in [response routes]) {
                [self.foodMapView addOverlay:[route polyline] level:MKOverlayLevelAboveRoads]; // Draws the route above roads, but below labels.
                // You can also get turn-by-turn steps, distance, advisory notices, ETA, etc by accessing various route properties.
            }
        }
    }];
    */
    
    //[mapManager ]
    
}

-(void)didTapSeeDetailsBtnInCalloutView:(id)sender{
    
    [self performSegueWithIdentifier:@"ShowFoodViaMap" sender:sender];
}

-(void)didTapCloseBtnInCalloutView:(id)sender{
    
    //[self.foodMapView deselectAnnotation:(id <MKAnnotation>)selectedAnnotationView animated:YES];
    for (NSObject<MKAnnotation> *annotation in [self.foodMapView selectedAnnotations]) {
        [self.foodMapView deselectAnnotation:(id <MKAnnotation>)annotation animated:NO];
    }
}




#pragma mark - MKMapViewDelegate

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
        
    } else if ([annotation isKindOfClass:[FoodAnnotation class]]) {
        
        static NSString * const identifier = @"annotationID";
        
        AnnotationView* annotationView = (AnnotationView *)[self.foodMapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (annotationView){
            
            annotationView.annotation = annotation;
        } else {
            annotationView = [[AnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        }
        
        
        annotationView.canShowCallout = NO; // set to YES if using customized rendition of standard callout; set to NO if creating your own callout from scratch
        annotationView.label.hidden = YES;
        annotationView.imageView.hidden = NO;
        //annotationView.imageView.image =
        
        FoodAnnotation *foodAnnotation = (FoodAnnotation *)annotation;
        NSString *foodImageURL = foodAnnotation.food.imageUrl;
       
        if(foodImageURL != nil){
            
            NSString * urlStr = [NSString stringWithFormat:@"%@%@",BaseURLString, foodImageURL];
            [annotationView.imageView sd_setImageWithURL:[NSURL URLWithString:urlStr]
                                  placeholderImage:[UIImage imageNamed:@"foodtruck"]];
        }else{
            
            annotationView.imageView.image = [UIImage imageNamed:@"foodtruck"];
        }
        
        return annotationView;
    }
    
    return nil;
}


-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    
    if ([view isKindOfClass:[AnnotationView class]]) {
        AnnotationView *annotationView = (AnnotationView *)view;
        selectedAnnotationView = annotationView;
        
        if ([annotationView.annotation isKindOfClass:[FoodAnnotation class]]) {
            FoodAnnotation *foodAnnotation = (FoodAnnotation *)annotationView.annotation;
            selectedFoodAnnotation = foodAnnotation;
            
            self.calloutView.foodNameLabel.text = foodAnnotation.food.name;
            self.calloutView.shopNameLabel.text = foodAnnotation.food.shopName;
            NSString *foodImageURL = foodAnnotation.food.imageUrl;
            if(foodImageURL != nil){
                
                NSString * urlStr = [NSString stringWithFormat:@"%@%@",BaseURLString, foodImageURL];
                [self.calloutView.imageView sd_setImageWithURL:[NSURL URLWithString:urlStr]
                                            placeholderImage:[UIImage imageNamed:@"foodtruck"]];
            }else{
                self.calloutView.imageView.image = [UIImage imageNamed:@"foodtruck"];
            }
            
            annotationView.calloutView = self.calloutView;
            
            [self.foodMapView setCenterCoordinate:foodAnnotation.coordinate animated:YES];
        }
    }
}

-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    
    if ([view isKindOfClass:[AnnotationView class]]) {
        AnnotationView *annotationView = (AnnotationView *)view;
        annotationView.calloutView = nil;
    }
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        [renderer setStrokeColor:[UIColor blueColor]];
        [renderer setLineWidth:5.0];
        return renderer;
    }
    return nil;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    DetailsViewController *vc = (DetailsViewController  *)segue.destinationViewController;
    vc.hidesBottomBarWhenPushed = YES;
    
    if ([segue.identifier isEqualToString:@"ShowFoodViaMap"])
    {
        vc.selectedFood = selectedFoodAnnotation.food;
    }
}

#pragma mark - GoogleAPIsDelegate

- (void)directionsCompletionCallback:(MKPolyline * _Nullable)route directionInformation:(NSDictionary * _Nullable)directionInformation boundingRegion:(NSDictionary * _Nullable)boundingRegion error:(NSString * _Nullable)error{
    
    if(error != nil){
        NSLog(@"%@", error);
    } else{
        
        MKPointAnnotation *pointOfOrigin = [[MKPointAnnotation alloc] init];
        pointOfOrigin.coordinate = route.coordinate;
        pointOfOrigin.title = (NSString *)directionInformation[@"start_address"];
        pointOfOrigin.subtitle = (NSString *)directionInformation[@"duration"];
        
        MKPointAnnotation *pointOfDestination = [[MKPointAnnotation alloc] init];
        pointOfDestination.coordinate = route.coordinate;
        pointOfDestination.title = (NSString *)directionInformation[@"end_address"];
        pointOfDestination.subtitle = (NSString *)directionInformation[@"distance"];
        
        NSDictionary *startLocation = (NSDictionary *)directionInformation[@"start_location"];
        NSDictionary *endLocation = (NSDictionary *)directionInformation[@"end_location"];
        
        CLLocationCoordinate2D coordOrigin = CLLocationCoordinate2DMake([(NSString *)startLocation[@"lat"] doubleValue], [(NSString *)startLocation[@"lng"] doubleValue]);
        CLLocationCoordinate2D coordDesitination = CLLocationCoordinate2DMake([(NSString *)endLocation[@"lat"] doubleValue], [(NSString *)endLocation[@"lng"] doubleValue]);
        
        pointOfOrigin.coordinate = coordOrigin;
        pointOfDestination.coordinate = coordDesitination;
        
        double x = [(NSString *)boundingRegion[@"x"] doubleValue];
        double y = [(NSString *)boundingRegion[@"y"] doubleValue];
        double w = [(NSString *)boundingRegion[@"w"] doubleValue];
        double h = [(NSString *)boundingRegion[@"h"] doubleValue];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self removeAllPlacemarkFromMap];
            //[self.foodMapView addOver ]
            drawnPolyLine = route;
            [self.foodMapView addOverlay:route level:MKOverlayLevelAboveRoads];
            [self.foodMapView addAnnotation:pointOfOrigin];
            [self.foodMapView addAnnotation:pointOfDestination];
            
            [self.foodMapView setVisibleMapRect:MKMapRectMake(x, y, w, h) animated:YES];
        });
    }
}

-(void)removeAllPlacemarkFromMap {
    
    for (id<MKAnnotation> annotation in self.foodMapView.annotations) {
        
        if (!([annotation isKindOfClass:[FoodAnnotation class]] || [annotation isKindOfClass:[MKUserLocation class]])) {
            
            [self.foodMapView removeAnnotation:annotation];
        }
    }
    
    [self.foodMapView removeOverlays: self.foodMapView.overlays];
}

@end

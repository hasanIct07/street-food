//
//  AddViewController.h
//  Street Food
//
//  Created by Mehedi Hasan on 1/14/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "ActionSheetPicker.h"
#import "BRPlaceholderTextView.h"
#import "MVPlaceSearchTextField.h"
#import "Food.h"

@interface AddViewController : UIViewController <PlaceSearchTextFieldDelegate, UITextFieldDelegate, UITextViewDelegate, MKMapViewDelegate>

//Public
@property BOOL isEditing;
@property (strong, nonatomic) Food *selectedFood;

@property CLLocationCoordinate2D selectedFoodCoordinate;
@property (strong, nonatomic) NSDate *selectedOpeningTime;
@property (strong, nonatomic) NSDate *selectedClosingTime;


//IBOutlet
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UITextField *foodNameTF;
@property (weak, nonatomic) IBOutlet UITextField *shopNameTF;
@property (weak, nonatomic) IBOutlet UITextField *priceTF;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *addressTF;
@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *descriptionTV;
@property (weak, nonatomic) IBOutlet UITextField *openTimeTF;
@property (weak, nonatomic) IBOutlet UITextField *closeTimeTF;


@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *addPhotoBtn;
@property (weak, nonatomic) IBOutlet UIImageView *uploadImageView;
@property (weak, nonatomic) IBOutlet UIButton *addFoodBtn;

- (IBAction)addPhotoBtnTapped:(id)sender;
- (IBAction)addFoodBtnTapped:(id)sender;

- (IBAction)openTimeTouched:(id)sender;


@end

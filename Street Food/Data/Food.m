//
//  Food.m
//  Street Food
//
//  Created by Mehedi Hasan on 1/27/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import "Food.h"

@implementation Food

- (instancetype)initwithServerData:(NSDictionary*)response{
    
    if (self == [super init]) {
        
        if (![[response valueForKey:@"FoodId"] isEqual:[NSNull null]]) {
            self.foodId= [response valueForKey:@"FoodId"];
        }
        if (![[response valueForKey:@"FoodName"] isEqual:[NSNull null]]) {
            self.name = [response valueForKey:@"FoodName"];
            
        }
        if (![[response valueForKey:@"Price"] isEqual:[NSNull null]]) {
            self.price = [response valueForKey:@"Price"];
            
        }
        if (![[response valueForKey:@"FoodImageUrl"] isEqual:[NSNull null]]) {
            self.imageUrl = [response valueForKey:@"FoodImageUrl"];
            
        }
        if (![[response valueForKey:@"address_line_2"] isEqual:[NSNull null]]) {
            self.foodDescription = [response valueForKey:@"address_line_2"];
            
        }
        if (![[response valueForKey:@"ShopName"] isEqual:[NSNull null]]) {
            self.shopName = [response valueForKey:@"ShopName"];
            
        }
        if (![[response valueForKey:@"ShopAddress"] isEqual:[NSNull null]]) {
            self.shopAddress = [response valueForKey:@"ShopAddress"];
            
        }
        if (![[response valueForKey:@"Latitude"] isEqual:[NSNull null]]) {
            self.latitude = [(NSNumber *)[response valueForKey:@"Latitude"] doubleValue];
            
        }
        if (![[response valueForKey:@"Latitude"] isEqual:[NSNull null]]) {
            self.longitude = [(NSNumber *)[response valueForKey:@"Longitude"] doubleValue];
            
        }
        if (![[response valueForKey:@"ShopOpeningTime"] isEqual:[NSNull null]]) {
            self.shopOpeningTime = [response valueForKey:@"ShopOpeningTime"];
            
        }
        if (![[response valueForKey:@"ShopClosingTime"] isEqual:[NSNull null]]) {
            self.shopClosingTime = [response valueForKey:@"ShopClosingTime"];
            
        }
        if (![[response valueForKey:@"CreateDate"] isEqual:[NSNull null]]) {
            self.createdDate = [response valueForKey:@"CreateDate"];
        }
        
        
        if (![[response valueForKey:@"AverageRating"] isEqual:[NSNull null]]) {
            self.averageRating = [(NSNumber *)[response valueForKey:@"AverageRating"] floatValue];
        }
        if (![[response valueForKey:@"AverageFoodRating"] isEqual:[NSNull null]]) {
            self.averageFoodRating = [(NSNumber *)[response valueForKey:@"AverageFoodRating"] floatValue];
        }
        if (![[response valueForKey:@"AveragePriceRating"] isEqual:[NSNull null]]) {
            self.averagePriceRating = [(NSNumber *)[response valueForKey:@"AveragePriceRating"] floatValue];
        }
        if (![[response valueForKey:@"AverageServiceRating"] isEqual:[NSNull null]]) {
            self.averageServiceRating = [(NSNumber *)[response valueForKey:@"AverageServiceRating"] floatValue];
        }
        if (![[response valueForKey:@"AverageDecoreRating"] isEqual:[NSNull null]]) {
            self.averageDecoreRating = [(NSNumber *)[response valueForKey:@"AverageDecoreRating"] floatValue];
        }
        
        if (![[response valueForKey:@"FavoriteCount"] isEqual:[NSNull null]]) {
            self.favoriteCount = [(NSNumber *)[response valueForKey:@"FavoriteCount"] intValue];
        }
        
    }
    return self;
    
}


@end

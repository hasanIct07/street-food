//
//  Food.h
//  Street Food
//
//  Created by Mehedi Hasan on 1/27/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Food : NSObject

@property (nonatomic, retain) NSNumber* foodId;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* price;
@property (nonatomic, retain) NSString* imageUrl;
@property (nonatomic, retain) NSString* foodDescription;

@property (nonatomic, retain) NSString* shopName;
@property (nonatomic, retain) NSString* shopAddress;
@property double latitude;
@property double longitude;
@property (nonatomic, retain) NSString* shopOpeningTime;
@property (nonatomic, retain) NSString* shopClosingTime;
@property (nonatomic, retain) NSString* createdDate;

@property float averageRating;
@property float averageFoodRating;
@property float averagePriceRating;
@property float averageServiceRating;
@property float averageDecoreRating;

@property int favoriteCount;


- (instancetype)initwithServerData:(NSDictionary*)response;

@end

//
//  ReviewViewController.m
//  Street Food
//
//  Created by Mehedi Hasan on 1/21/17.
//  Copyright © 2017 BUET. All rights reserved.
//

#import "ReviewViewController.h"
#import "APIManager.h"

@interface ReviewViewController ()

@end

@implementation ReviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.title = @"Add Review";
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc]
                                initWithTitle:@""
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:nil];
    self.navigationController.navigationBar.topItem.backBarButtonItem = btnBack;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelBtnTap:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)submitBtnTap:(id)sender{
    
    
    NSDictionary *foodParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"1", @"UserId",
                                self.selectedFood.foodId, @"FoodId",
                                @(self.foodRatingView.value), @"FoodRating",
                                @(self.priceRatingView.value), @"PriceRating",
                                @(self.serviceRatingView.value), @"ServiceRating",
                                @(self.decoreRatingView.value), @"DecoreRating",
                                self.descriptionTV.text, @"ReviewText", nil];
    
    
    [[APIManager sharedManager] addFoodReviewWithParam:foodParams Completion:^(NSString *message, BOOL success) {
        
        if(success){
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
